#==============================================================================
add_subdirectory(Common)
#==============================================================================
paraview_add_plugin(Mockdatasource
        VERSION "0.1.118"
        MODULES PluginMockdatasource
        MODULE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/PluginMockdatasource/vtk.module")
#==============================================================================
