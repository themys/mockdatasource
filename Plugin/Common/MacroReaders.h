//
// File MacroReaders.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#ifndef MACRO_READERS_H
#define MACRO_READERS_H
//--------------------------------------------------------------------------------------------------
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <thread>
#include <vector>
//--------------------------------------------------------------------------------------------------
#include <stdlib.h>
//--------------------------------------------------------------------------------------------------
#define STAT_WARNING_ENUM 0
#define STAT_ASSERT_ENUM 1
//--------------------------------------------------------------------------------------------------
#define STAT_TYPE_MSG(typemsg, msg)                                                                \
  {                                                                                                \
    bool _tracing = false;                                                                         \
    const std::vector<std::string> _msg_tracing = { "WARNING", "ASSERT" };                         \
    switch (typemsg)                                                                               \
    {                                                                                              \
      case STAT_WARNING_ENUM:                                                                      \
      {                                                                                            \
        if (_stat_chatter_warning)                                                                 \
          _tracing = true;                                                                         \
        break;                                                                                     \
      }                                                                                            \
      case STAT_ASSERT_ENUM:                                                                       \
      {                                                                                            \
        _tracing = true;                                                                           \
      }                                                                                            \
      default:                                                                                     \
      {                                                                                            \
        break;                                                                                     \
      }                                                                                            \
    }                                                                                              \
    if (_tracing)                                                                                  \
    {                                                                                              \
      std::cerr << "### " << _msg_tracing[typemsg] << " ### [" << stat_class_fct << _stat_para     \
                << "] " << msg << std::endl;                                                       \
    }                                                                                              \
  }
//--------------------------------------------------------------------------------------------------
#define STAT_WARNING(msg) STAT_TYPE_MSG(STAT_WARNING_ENUM, msg)
//--------------------------------------------------------------------------------------------------
// TODO
//  vtkErrorMacro( << e.what() << "\n");
#define STAT_ASSERT(cnd, msg)                                                                      \
  if (!(cnd))                                                                                      \
  {                                                                                                \
    STAT_TYPE_MSG(STAT_ASSERT_ENUM, msg)                                                           \
    STAT_TYPE_MSG(STAT_ASSERT_ENUM, __FILE__ << ":" << __LINE__)                                   \
    exit(1);                                                                                       \
  }
//--------------------------------------------------------------------------------------------------
#define VTK_MACRO_READERS_STAT_COUT std::cerr
#define VTK_MACRO_READERS_STAT_CERR std::cerr
//--------------------------------------------------------------------------------------------------
#ifdef VTK_MACRO_READERS_STAT
// CASE compiled WITH +verbose
//--------------------------------------------------------------------------------------------------
#define VTK_MACRO_READERS_STAT_WITH_PID false
//--------------------------------------------------------------------------------------------------

// TODO
// 			vtkNew<vtkTimerLog> timer;
//			timer->StartTimer( );
//				timer->StopTimer( );
//				cout << timer->GetElapsedTime( ) << " s" << endl;

// TODO
// this->UpdateProgress() une valeur de 0 a 1 a la fin

#define STAT_CND true
#define STAT_FINE_CND false

// TODO Ajouter une configuration par défaut :
//  -HerculeDataSourceImpl::initDerivedType

#define STRACING(dec, msg)                                                                         \
  if (_tracing)                                                                                    \
  {                                                                                                \
    VTK_MACRO_READERS_STAT_COUT << "@@@@@ " << dec << msg << std::endl;                            \
  }

class STAT_CLASS
{
protected:
  mutable std::string m_stat_para = "";
  mutable bool m_stat_first = true;
  mutable bool m_tracing = false;
  mutable bool m_stat_chatter = true;
  mutable bool m_stat_chatter_fine = false;
  mutable std::string m_pattern_fct;

  static bool startBy(const std::string& str, const std::string& from)
  {
    // std::cout << " length:" << str.length() << " <?< " << from.length();
    if (str.length() < from.length())
    {
      return false;
    }
    // std::cout << " str:" << str.substr(0, from.length()) << " =?= from:" << from;
    return str.substr(0, from.length()) == from;
  }

public:
  STAT_CLASS() {}
  ~STAT_CLASS() {}

  void SetStatPara(const std::string& _stat_para) const { m_stat_para = _stat_para; }
  const std::string GetStatPara(void) const { return m_stat_para; }

  static bool Match(bool _tracing, const std::string& _str, std::string _pattern)
  {
    STRACING("      ", "MATCH str: " << _str << " pattern: " << _pattern)
    if (_pattern[_pattern.length() - 1] == '*')
    {
      _pattern = _pattern.substr(0, _pattern.length() - 1);
      if (_pattern == "")
      {
        STRACING("      ", "MATCH RETURN " << true << " pattern: " << _pattern)
        return true;
      }
      STRACING("      ", "MATCH str: " << _str << " pattern: " << _pattern << " with * ")
      if (_str.length() < _pattern.length())
      {
        STRACING("      ",
          "MATCH RETURN " << false << " str ##" << _str.length() << " ?<? pattern ##"
                          << _pattern.length())
        return false;
      }
      STRACING("      ", "MATCH str ##" << _str.length() << " >= pattern ##" << _pattern.length())
      STRACING("      ",
        "MATCH RETURN " << (_str.substr(0, _pattern.length()) == _pattern) << " "
                        << _str.substr(0, _pattern.length()) << " =?= " << _pattern)
      return _str.substr(0, _pattern.length()) == _pattern;
    }
    STRACING("      ", "MATCH RETURN " << (_str == _pattern) << " " << _str << " =?= " << _pattern)
    return (_str == _pattern);
  }

  static void InitChatterStatic(bool _tracing,
    const std::string& _clss,
    bool& _stat_chatter,
    bool& _stat_chatter_fine,
    std::string& _pattern_fct)
  {
    _stat_chatter = false;
    _stat_chatter_fine = false;
    _pattern_fct = "*";
    STRACING("   ", "ICS clss: " << _clss)
    _stat_chatter = valueVarEnvBoolean("STAT_STATUS");
    STRACING("   ", "ICS STAT_STATUS: " << _stat_chatter)
    if (std::getenv("STAT_ON"))
    {
      STRACING("   ", "ICS STAT_ON: " << _stat_chatter)
      _stat_chatter = true;
    }
    else
    {
      STRACING("   ", "ICS STAT_ON: " << false)
    }
    const char* var_env = std::getenv("STAT");
    STRACING("   ", "ICS STAT: " << (var_env ? var_env : "nullptr"))
    if (var_env)
    {
      std::string tokens(var_env);
      // List tokens
      size_t pos = 0;
      std::vector<std::string> list_tokens;
      while ((pos = tokens.find(";")) != std::string::npos)
      {
        std::string token = tokens.substr(0, pos);
        if (token == "")
        {
          continue;
        }
        tokens = tokens.substr(pos + 1);
        STRACING("   ", "ICS detect token: " << token)
        list_tokens.emplace_back(token);
      }
      STRACING("   ", "ICS detect token: " << tokens)
      list_tokens.emplace_back(tokens);
      // Use tokens
      for (const auto& token : list_tokens)
      {
        STRACING("   ", "ICS use token: " << token)
        _pattern_fct = "*";
        bool state = true;
        std::size_t deb = 0;
        switch (token[0])
        {
          case '-':
          case '~':
          {
            state = false;
            deb = 1;
            STRACING("   ", "ICS detect (-|~) state: " << state << " deb: " << deb)
            break;
          }
          case '+':
          {
            state = true;
            deb = 1;
            STRACING("   ", "ICS detect (+) state: " << state << " deb: " << deb)
            break;
          }
          default:
          {
            state = true;
            deb = 0;
            STRACING("   ", "ICS detect () state: " << state << " deb: " << deb)
            break;
          }
        }
        pos = token.find(":");
        std::string pattern = token.substr(deb, pos - deb);
        STRACING("   ",
          "ICS token: " << token << " deb: " << deb << " pos: " << pos
                        << " -> pattern: " << pattern)
        if (pos != std::string::npos)
        {
          // case <clss>:<fct>
          _pattern_fct = token.substr(pos + 1);
          STRACING("   ",
            "ICS (first \":\") : " << token << " deb: " << pos + 1
                                   << " -> pattern_fct: " << _pattern_fct)
          pos = _pattern_fct.find(":");
          if (pos != std::string::npos)
          {
            if (pos != 0)
            {
              std::cout << "### WARNING ### Ignored token: " << token
                        << " in STAT. The token cannot include \":\" or \"::\" once as a separator "
                           "between the name of the class and the name of the function."
                        << std::endl;
              continue;
            }
            // case <clss>::<fct>
            _pattern_fct = _pattern_fct.substr(1);
            STRACING("   ", "ICS (second \":\") pattern_fct: " << _pattern_fct)
          }
        }
        if (Match(_tracing, _clss, pattern))
        {
          _stat_chatter = state;
          STRACING(
            "   ", "ICS RETURN chatter: " << _stat_chatter << " pattern_fct: " << _pattern_fct)
          return;
        }
      }
    }
    _pattern_fct = "*";
    STRACING("   ", "ICS RETURN chatter: " << _stat_chatter << " pattern_fct: " << _pattern_fct)
    return;
  }

  static bool valueVarEnvBoolean(const std::string& _env)
  {
    if (_env == "")
    {
      return false;
    }
    const char* val = std::getenv(_env.c_str());
    if (val == nullptr)
    {
      return false;
    }
    std::string sval(val);
    const std::map<std::string, bool> know = { { "", true },
      { "1", true },
      { "on", true },
      { "On", true },
      { "true", true },
      { "True", true },
      { "0", false },
      { "off", false },
      { "Off", false },
      { "false", false },
      { "False", false } };
    const auto& it = know.find(sval);
    if (it == know.end())
    {
      VTK_MACRO_READERS_STAT_CERR << "### WARNING ### Ignored environment variable: " << _env
                                  << " cause non-interpretable value " << sval
                                  << " (nothing,1,on,On,true,True for true or"
                                  << " 0,off,Off,false,False)" << std::endl;
      return false;
    }
    bool tracing = false;
    if (std::string(_env) == "STAT_VERBOSE")
    {
      tracing = it->second;
    }
    else
    {
      tracing = valueVarEnvBoolean("STAT_VERBOSE");
    }
    if (tracing)
    {
      VTK_MACRO_READERS_STAT_COUT << "Boolean environement variable " << _env << " is "
                                  << it->second << std::endl;
    }
    return it->second;
  }

  void InitChatter(const std::string& _clss,
    const std::string& _fct,
    bool& _stat_chatter,
    bool& _stat_chatter_fine) const
  {
    bool _tracing = this->m_tracing;
    if (this->m_stat_first)
    {
      _tracing = valueVarEnvBoolean("STAT_VERBOSE");
      this->m_tracing = _tracing;
      STRACING("", "IC clss: " << _clss << " fct: " << _fct)
      this->m_stat_first = false;
      STRACING("", "IC STAT_VERBOSE: " << this->m_tracing)
      InitChatterStatic(this->m_tracing,
        _clss,
        this->m_stat_chatter,
        this->m_stat_chatter_fine,
        this->m_pattern_fct);
    }
    STRACING("", "IC clss: " << _clss << " fct: " << _fct)
    bool ret1 = (this->m_stat_chatter && this->m_pattern_fct == "*");
    STRACING("", "   RETURN " << ret1)
    bool ret2 = (this->m_stat_chatter && Match(this->m_tracing, _fct, this->m_pattern_fct));
    STRACING("", "       || " << ret2)
    bool ret3 = (!this->m_stat_chatter && !Match(this->m_tracing, _fct, this->m_pattern_fct));
    STRACING("", "       || " << ret3)
    _stat_chatter = ret1 || ret2 || ret3;
    _stat_chatter_fine = this->m_stat_chatter_fine;
  }
};

// ajouter une option chatter
#define STAT_TIC(clss, fct)                                                                        \
  std::string stat_class_fct = std::string(clss) + "::" + fct;                                     \
  std::string _stat_para = GetStatPara();                                                          \
  bool _stat_chatter_warning = valueVarEnvBoolean("STAT_WARNING");                                 \
  bool _stat_chatter = false;                                                                      \
  bool _stat_chatter_fine = false;                                                                 \
  InitChatter(clss, fct, _stat_chatter, _stat_chatter_fine);                                       \
  auto stat_class_fct_tic = std::chrono::system_clock::now();                                      \
  if (_stat_chatter)                                                                               \
  {                                                                                                \
    VTK_MACRO_READERS_STAT_COUT << "IN  [" << stat_class_fct << _stat_para << "]" << std::endl;    \
  }

// la version pour methode isolee, sans stat_para
#define STAT_TIC_MET(clss, fct, stat_para)                                                         \
  std::string stat_class_fct = std::string(clss) + "::" + fct;                                     \
  std::string _stat_para = stat_para;                                                              \
  bool _stat_tracing = STAT_CLASS::valueVarEnvBoolean("STAT_VERBOSE");                             \
  bool _stat_chatter_warning = STAT_CLASS::valueVarEnvBoolean("STAT_WARNING");                     \
  bool _stat_chatter = false;                                                                      \
  bool _stat_chatter_fine = false;                                                                 \
  std::string _pattern_fct;                                                                        \
  STAT_CLASS::InitChatterStatic(                                                                   \
    _stat_tracing, clss, _stat_chatter, _stat_chatter_fine, _pattern_fct);                         \
  if (_stat_chatter)                                                                               \
  {                                                                                                \
    _stat_chatter = STAT_CLASS::Match(_stat_tracing, fct, _pattern_fct);                           \
  }                                                                                                \
  auto stat_class_fct_tic = std::chrono::system_clock::now();                                      \
  if (_stat_chatter)                                                                               \
  {                                                                                                \
    VTK_MACRO_READERS_STAT_COUT << "IN  [" << stat_class_fct << _stat_para << "]" << std::endl;    \
  }

#define STAT_TAC                                                                                   \
  {                                                                                                \
    auto stat_class_fct_tac = std::chrono::system_clock::now();                                    \
    std::chrono::duration<double> elapsed_seconds = stat_class_fct_tac - stat_class_fct_tic;       \
    if (_stat_chatter)                                                                             \
    {                                                                                              \
      VTK_MACRO_READERS_STAT_COUT << "OUT [" << stat_class_fct << _stat_para                       \
                                  << "] TimeElapse:" << elapsed_seconds.count() << " s"            \
                                  << std::endl;                                                    \
    }                                                                                              \
  }

#define STAT(msg)                                                                                  \
  if (_stat_chatter)                                                                               \
  {                                                                                                \
    VTK_MACRO_READERS_STAT_COUT << "    [" << stat_class_fct << _stat_para << "] " << msg          \
                                << std::endl;                                                      \
  }

#define STAT_FINE(msg)                                                                             \
  if (_stat_chatter_fine)                                                                          \
  {                                                                                                \
    VTK_MACRO_READERS_STAT_COUT << "    [" << stat_class_fct << _stat_para << "] " << msg          \
                                << std::endl;                                                      \
  }

#define STAT_MIN_MAX(type, vals, nbVals, msg)                                                      \
  if (STAT_CND && nbVals != 0)                                                                     \
  {                                                                                                \
    STAT(                                                                                          \
      "STAT_MIN_MAX " << #type << " " << #vals << " " << #nbVals << ":" << nbVals << " " << #msg)  \
    type min = vals[0];                                                                            \
    type max = vals[0];                                                                            \
    for (long int i = 0; i < nbVals; ++i)                                                          \
    {                                                                                              \
      if (vals[i] < min)                                                                           \
        min = vals[i];                                                                             \
      if (max < vals[i])                                                                           \
        max = vals[i];                                                                             \
    }                                                                                              \
    STAT(msg << " nb:" << nbVals << " [min: " << min << ", max: " << max << "]")                   \
  }

#define STAT_VECTOR(type, vals, msg)                                                               \
  if (STAT_CND)                                                                                    \
  {                                                                                                \
    STAT("STAT_VECTOR " << #type << " " << #vals << " " << msg)                                    \
    if (vals.size() != 0)                                                                          \
    {                                                                                              \
      std::string sdatas;                                                                          \
      double sum = 0;                                                                              \
      unsigned long cptmin = 0;                                                                    \
      type min = vals[0];                                                                          \
      unsigned long cptmax = 0;                                                                    \
      type max = vals[0];                                                                          \
      bool tracing = false;                                                                        \
      for (const auto& it : vals)                                                                  \
      {                                                                                            \
        if (tracing)                                                                               \
        {                                                                                          \
          sdatas += std::to_string(it) + " ";                                                      \
        }                                                                                          \
                                                                                                   \
        sum += it;                                                                                 \
                                                                                                   \
        if (it < min)                                                                              \
        {                                                                                          \
          min = it;                                                                                \
          cptmin = 1;                                                                              \
        }                                                                                          \
        else if (it == min)                                                                        \
        {                                                                                          \
          ++cptmin;                                                                                \
        }                                                                                          \
                                                                                                   \
        if (max < it)                                                                              \
        {                                                                                          \
          max = it;                                                                                \
          cptmax = 1;                                                                              \
        }                                                                                          \
        else if (max == it)                                                                        \
        {                                                                                          \
          ++cptmax;                                                                                \
        }                                                                                          \
      }                                                                                            \
      STAT("STAT_VECTOR " << msg << ": nb:" << vals.size() << " [min: " << min << " (#" << cptmin  \
                          << "), max: " << max << " (#" << cptmax << ")]: "                        \
                          << " sum:" << sum << " avg:" << sum / vals.size() << " " << sdatas)      \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      STAT("STAT_VECTOR " << msg << ": nb:" << vals.size() << " == 0")                             \
    }                                                                                              \
  }

#define STAT_BOOL_VECTOR(vals, msg)                                                                \
  if (STAT_CND && vals.size())                                                                     \
  {                                                                                                \
    unsigned long cpttrue = 0;                                                                     \
    unsigned long cptfalse = 0;                                                                    \
    for (long int i = 0; i < vals.size(); ++i)                                                     \
    {                                                                                              \
      if (vals[i])                                                                                 \
      {                                                                                            \
        ++cpttrue;                                                                                 \
      }                                                                                            \
      else                                                                                         \
      {                                                                                            \
        ++cptfalse;                                                                                \
      }                                                                                            \
    }                                                                                              \
    STAT(                                                                                          \
      msg << " nb:" << vals.size() << " [true: #" << cpttrue << ", false: #" << cptfalse << "]")   \
  }

#define STAT_VTK_MIN_MAX(type, vals, nbVals, msg)                                                  \
  if (STAT_CND && nbVals != 0)                                                                     \
  {                                                                                                \
    STAT("STAT_VTK_MIN_MAX " << #type << " " << #vals << " " << #nbVals << ":" << nbVals << " "    \
                             << #msg)                                                              \
    type min = vals->GetValue(0);                                                                  \
    type max = vals->GetValue(0);                                                                  \
    STAT(nbVals << "=?=" << vals->GetNumberOfValues() << " (" << vals->GetNumberOfTuples() << "*"  \
                << vals->GetNumberOfComponents())                                                  \
    for (long int i = 0; i < nbVals; ++i)                                                          \
    {                                                                                              \
      if (vals->GetValue(i) < min)                                                                 \
        min = vals->GetValue(i);                                                                   \
      if (max < vals->GetValue(i))                                                                 \
        max = vals->GetValue(i);                                                                   \
    }                                                                                              \
    STAT(msg << " nb:" << nbVals << " [min: " << min << ", max: " << max << "]")                   \
  }

#define STAT_RETURN(ret)                                                                           \
  {                                                                                                \
    STAT_TAC                                                                                       \
    return ret;                                                                                    \
  }

#define STAT_RETURN_PRINT(ret, msg)                                                                \
  {                                                                                                \
    STAT("RETURN: " << msg)                                                                        \
    STAT_TAC                                                                                       \
    return ret;                                                                                    \
  }

#define STAT_RETURN_TYPE_PRINT(rettype, ret, msg)                                                  \
  {                                                                                                \
    rettype willReturn = ret;                                                                      \
    STAT("RETURN: " << willReturn << " (" << #rettype << ") " << msg)                              \
    STAT_TAC                                                                                       \
    return willReturn;                                                                             \
  }

#define STAT_INPUT(var, val) STAT("INPUT PARAM: " << var << " = " << val)

#define STAT_OUTPUT(var, val) STAT("OUTPUT PARAM: " << var << " = " << val)

// TODO C++ is bullshit why don't do std::to_string(std::this_thread::get_id()) ?
#define STAT_SET_PARA(idProc, nbProc)                                                              \
  if (GetStatPara().size() == 0)                                                                   \
  {                                                                                                \
    if (VTK_MACRO_READERS_STAT_WITH_PID)                                                           \
    {                                                                                              \
      std::ostringstream oss;                                                                      \
      oss << std::this_thread::get_id();                                                           \
      SetStatPara("@" + std::to_string(idProc) + "/" + std::to_string(nbProc) + " " + oss.str());  \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      SetStatPara("@" + std::to_string(idProc) + "/" + std::to_string(nbProc));                    \
    }                                                                                              \
    _stat_para = GetStatPara();                                                                    \
    STAT("");                                                                                      \
  }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#else // VTK_MACRO_READERS_STAT
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// CASE compiled WITHOUT +verbose
class STAT_CLASS
{
public:
  void SetStatPara(const std::string& _stat_para) {}
  const std::string GetStatPara(void) const { return ""; }

  static bool valueVarEnvBoolean(const std::string& _env)
  {
    if (_env == "")
    {
      return false;
    }
    const char* val = std::getenv(_env.c_str());
    if (val == nullptr)
    {
      return false;
    }
    std::string sval(val);
    const std::map<std::string, bool> know = { { "", true },
      { "1", true },
      { "on", true },
      { "On", true },
      { "true", true },
      { "True", true },
      { "0", false },
      { "off", false },
      { "Off", false },
      { "false", false },
      { "False", false } };
    const auto& it = know.find(sval);
    if (it == know.end())
    {
      VTK_MACRO_READERS_STAT_CERR << "### WARNING ### Ignored environment variable: " << _env
                                  << " cause non-interpretable value " << sval
                                  << " (nothing,1,on,On,true,True for true or"
                                  << " 0,off,Off,false,False)" << std::endl;
      return false;
    }
    bool tracing;
    if (std::string(_env) == "STAT_VERBOSE")
    {
      tracing = it->second;
    }
    else
    {
      tracing = valueVarEnvBoolean("STAT_VERBOSE");
    }
    if (tracing)
    {
      VTK_MACRO_READERS_STAT_COUT << "Boolean environement variable " << _env << " is "
                                  << it->second << std::endl;
    }
    return it->second;
  }

  bool InitChatter(const std::string& clss, const std::string& fct) { return false; }
};

#define STAT_CND false
#define STAT_FINE_CND false
#define STAT_TIC(clss, fct)                                                                        \
  bool _stat_chatter_warning = valueVarEnvBoolean("STAT_WARNING");                                 \
  std::string stat_class_fct, _stat_para;
#define STAT_TIC_MET(clss, fct, stat_para)                                                         \
  bool _stat_chatter_warning = STAT_CLASS::valueVarEnvBoolean("STAT_WARNING");                     \
  std::string stat_class_fct, _stat_para;
#define STAT_TAC
#define STAT(msg)
#define STAT_FINE(msg)
#define STAT_MIN_MAX(type, vals, nbVals, msg)
#define STAT_VECTOR(type, vals, msg)
#define STAT_BOOL_VECTOR(vals, msg)
#define STAT_VTK_MIN_MAX(type, vals, nbVals, msg)

#define STAT_RETURN(ret)                                                                           \
  {                                                                                                \
    STAT_TAC                                                                                       \
    return ret;                                                                                    \
  }
#define STAT_RETURN_PRINT(ret, msg) STAT(msg) STAT_RETURN(ret)
#define STAT_RETURN_TYPE_PRINT(rettype, ret, msg) STAT_RETURN(ret)
#define STAT_INPUT(var, val)
#define STAT_OUTPUT(var, val)

#define STAT_SET_PARA(idProc, nbProc)

//--------------------------------------------------------------------------------------------------
#endif // VTK_MACRO_READERS_STAT

#define STAT_CND_RETURN(cnd, ret)                                                                  \
  if (cnd)                                                                                         \
  {                                                                                                \
    STAT_RETURN(ret)                                                                               \
  }

#define STAT_CND_RETURN_PRINT(cnd, ret, msg)                                                       \
  if (cnd)                                                                                         \
  {                                                                                                \
    STAT_RETURN_PRINT(ret, msg)                                                                    \
  }

#define STAT_CND_RETURN_TYPE_PRINT(type, cnd, ret, msg)                                            \
  if (cnd)                                                                                         \
  {                                                                                                \
    STAT_RETURN_TYPE_PRINT(type, ret, msg)                                                         \
  }

#define STAT_RETURN_BOOL_PRINT(ret, msg) STAT_RETURN_TYPE_PRINT(bool, ret, msg)

#define STAT_CND_RETURN_BOOL_PRINT(cnd, ret, msg) STAT_CND_RETURN_TYPE_PRINT(bool, cnd, ret, msg)

#define STAT_GURU(cnd) STAT_ASSERT(cnd, " Guru meditation! Internal logic error.")

#define STAT_CND_WARNING(cnd, msg)                                                                 \
  if (cnd)                                                                                         \
  {                                                                                                \
    STAT_WARNING(msg)                                                                              \
  }

#define STAT_NOT_IMPLEMNTED_CASE(cnd)                                                              \
  STAT_ASSERT(!(cnd), " Guru meditation by not implemented! Internal logic error.")

#define STAT_GURU_EQ(vl, vd)                                                                       \
  STAT_ASSERT(vl == vd, "Guru meditation! Internal logic error: " << vl << " == " << vd)

#define STAT_GURU_NE(vl, vd)                                                                       \
  STAT_ASSERT(vl != vd, "Guru meditation! Internal logic error: " << vl << " != " << vd)

#endif // MACRO_READERS_H