//
// File vtkMockdatasource.cxx
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020-22 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#include "vtkMockdatasource.h"
#include "../Common/MacroReaders.h"

#include "MockDataSourceImpl.h"

#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkMultiProcessController.h>
#include <vtkObjectFactory.h>
#include <vtkStreamingDemandDrivenPipeline.h>

vtkStandardNewMacro(vtkMockdatasource);
//--------------------------------------------------------------------------------------------------
// In OptionArraySelection
// Triple definition : vtkMockdatasource.cxx, MockDataSource.cxx and HerculeDataSourceImpl.h
#define CHANGED_STATUS_WITH_RELOAD "Changed with reload"
//--------------------------------------------------------------------------------------------------
void vtkMockdatasource::setOptions()
{
  STAT_TIC("vtkReaders", "setOptions")

  if (!this->m_data_source)
  {
    STAT("No data source")
    this->m_data_source =
      std::make_shared<MockDataSourceImpl>(this->m_bool_option, this->m_int_option);
    this->m_data_source->SetAlgorithm(static_cast<vtkAlgorithm*>(this));
  }

  for (std::size_t iOption = 0; iOption < this->OptionArraySelection->GetNumberOfArrays();
       ++iOption)
  {
    const char* name = this->OptionArraySelection->GetArrayName(iOption);
    this->m_bool_option[name] = this->OptionArraySelection->ArrayIsEnabled(name);
  }
  this->m_data_source->SetOptions(this->m_bool_option, this->m_int_option);
  for (const auto& [key, value] : this->m_bool_option)
  {
    this->OptionArraySelection->AddArray(key.c_str());
    if (value)
    {
      this->OptionArraySelection->EnableArray(key.c_str());
    }
    else
    {
      this->OptionArraySelection->DisableArray(key.c_str());
    }
    if (key == CHANGED_STATUS_WITH_RELOAD &&
      value != this->m_previous_bool_option[CHANGED_STATUS_WITH_RELOAD])
    {
      STAT("Changed " CHANGED_STATUS_WITH_RELOAD)
      this->m_previous_bool_option[CHANGED_STATUS_WITH_RELOAD] =
        this->m_bool_option[CHANGED_STATUS_WITH_RELOAD];
      this->SetReLoad(true);
      STAT("SetReLoad(true)")
      this->Modified();
      STAT("modified")
    }
  }
  STAT_TAC
}
//--------------------------------------------------------------------------------------------------
void vtkMockdatasource::ActiveReload()
{
  STAT_TIC("vtkMockdatasource", "ActiveReLoad")
  this->SetReLoad(true);
  STAT_TAC
}
//--------------------------------------------------------------------------------------------------
void vtkMockdatasource::ActiveRefreshInformation(){ STAT_TIC("vtkMockdatasource",
  "ActiveRefreshInformation") STAT_TAC }
//--------------------------------------------------------------------------------------------------
vtkMockdatasource::vtkMockdatasource()
{
  STAT_TIC("vtkMockdatasource", "vtkMockdatasource")
  this->m_version = "0.1.128 " + std::string(__DATE__) + " " + __TIME__;
  this->SetVersion(this->m_version.c_str());
  this->AllSimulationTimes = vtkSmartPointer<vtkStringArray>::New();
  vtkMultiProcessController* controller = vtkMultiProcessController::GetGlobalController();
  STAT("Controller:" << controller)
  if (controller)
  {
    this->m_pe_nb = controller->GetNumberOfProcesses();
    this->m_pe_rank = controller->GetLocalProcessId();
  }
  STAT_SET_PARA(this->m_pe_rank, this->m_pe_nb);
  this->PointArraySelection = vtkSmartPointer<vtkDataArraySelection>::New();
  this->CellArraySelection = vtkSmartPointer<vtkDataArraySelection>::New();
  this->MaterialArraySelection = vtkSmartPointer<vtkDataArraySelection>::New();
  this->MeshArraySelection = vtkSmartPointer<vtkDataArraySelection>::New();
  this->OptionArraySelection = vtkSmartPointer<vtkDataArraySelection>::New();
  STAT_TAC
}
//--------------------------------------------------------------------------------------------------
vtkMockdatasource::~vtkMockdatasource()
{
  STAT_TIC("vtkMockdatasource", "~vtkMockdatasource")
  STAT_TAC
}
//--------------------------------------------------------------------------------------------------
void vtkMockdatasource::SetSimulationTimes(const char* dimensions)
{
  STAT_TIC("vtkMockdatasource", "SetSimulationTimes")
  STAT_INPUT("dimensions", dimensions)
  if (std::string(dimensions) == std::string())
  {
    IndexSimulationTime = -1;
  }
  else
  {
    std::string sdim(dimensions);
    std::size_t sepa = sdim.find(':');
    std::string sindex = sdim.substr(0, sepa);
    IndexSimulationTime = stoi(sindex);
  }
  STAT_OUTPUT("IndexSimulationTime", IndexSimulationTime)
  this->Modified();
  STAT_TAC
}
//--------------------------------------------------------------------------------------------------
int vtkMockdatasource::CanReadFile(const char* fileName)
{
  STAT_TIC("vtkMockdatasource", "CanReadFile")
  STAT_INPUT("fileName", fileName)

  this->setOptions();

  STAT_RETURN_TYPE_PRINT(int, (this->m_data_source->CanReadFile(fileName) ? 3 : 0), "")
}
//--------------------------------------------------------------------------------------------------
void vtkMockdatasource::GetTimeStepRange(int& t1, int& t2)
{
  STAT_TIC("vtkMockdatasource", "GetTimeStepRange")
  t1 = 0;
  t2 = this->m_times.size() - 1;
  STAT_OUTPUT("t1", t1)
  STAT_OUTPUT("t2", t2)
  STAT_TAC
}

void vtkMockdatasource::GetTimeStepRange(int tr[2])
{
  STAT_TIC("vtkMockdatasource", "GetTimeStepRange")
  this->GetTimeStepRange(tr[0], tr[1]);
  STAT_OUTPUT("tr", "[" << tr[0] << "," << tr[1] << "]")
  STAT_TAC
}

int* vtkMockdatasource::GetTimeStepRange()
{
  STAT_TIC("vtkReader", "GetTimeStepRange")
  static int tr[2];
  this->GetTimeStepRange(tr);
  STAT_RETURN_PRINT(tr, "")
}
//--------------------------------------------------------------------------------------------------
void vtkMockdatasource::GetTimeRange(double tr[2])
{
  STAT_TIC("vtkMockdatasource", "GetTimeRange")
  int* tr_ind = this->GetTimeStepRange();
  tr[0] = this->m_times[tr_ind[0]];
  tr[1] = this->m_times[tr_ind[1]];
  STAT_OUTPUT("tr", "[" << tr[0] << "," << tr[1] << "]")
  STAT_TAC
}
//--------------------------------------------------------------------------------------------------
int vtkMockdatasource::GetNumberOfPointArrays()
{
  STAT_TIC("vtkMockdatasource", "GetNumberOfPointArrays")
  STAT_RETURN_TYPE_PRINT(int, this->PointArraySelection->GetNumberOfArrays(), "")
}

const char* vtkMockdatasource::GetPointArrayName(int index)
{
  STAT_TIC("vtkMockdatasource", "GetPointArrayName")
  STAT_INPUT("index", index)
  STAT_RETURN_PRINT(this->PointArraySelection->GetArrayName(index),
    "RETURN: " + std::string(this->PointArraySelection->GetArrayName(index)))
}

void vtkMockdatasource::SetPointArrayStatus(const char* name, int status)
{
  STAT_TIC("vtkMockdatasource", "SetPointArrayStatus")
  STAT_INPUT("name", name)
  STAT_INPUT("status", status)
  int prevStatus = this->PointArraySelection->ArrayIsEnabled(name);
  STAT("prevStatus: " << prevStatus)
  STAT_CND_RETURN_PRINT(prevStatus == status, , "no modified")
  if (status)
  {
    this->PointArraySelection->EnableArray(name);
    int newStatus = this->PointArraySelection->ArrayIsEnabled(name);
    STAT("EnableArray newStatus: " << newStatus)
  }
  else
  {
    this->PointArraySelection->DisableArray(name);
    int newStatus = this->PointArraySelection->ArrayIsEnabled(name);
    STAT("DisableArray newStatus: " << newStatus)
  }
  this->Modified();
  STAT("modified")
  STAT_TAC
}

int vtkMockdatasource::GetPointArrayStatus(const char* name)
{
  STAT_TIC("vtkMockdatasource", "GetPointArrayStatus")
  STAT_INPUT("name", name)
  STAT_RETURN_TYPE_PRINT(int, this->PointArraySelection->ArrayIsEnabled(name), "")
}
//--------------------------------------------------------------------------------------------------
int vtkMockdatasource::GetNumberOfCellArrays()
{
  STAT_TIC("vtkMockdatasource", "GetNumberOfCellArrays")
  STAT_RETURN_TYPE_PRINT(int, this->CellArraySelection->GetNumberOfArrays(), "")
}

const char* vtkMockdatasource::GetCellArrayName(int index)
{
  STAT_TIC("vtkMockdatasource", "GetCellArrayName")
  STAT_INPUT("index", index)
  STAT_RETURN_PRINT(this->CellArraySelection->GetArrayName(index),
    std::string(this->CellArraySelection->GetArrayName(index)))
}

void vtkMockdatasource::SetCellArrayStatus(const char* name, int status)
{
  STAT_TIC("vtkMockdatasource", "SetCellArrayStatus")
  STAT_INPUT("name", name)
  STAT_INPUT("status", status)
  int prevStatus = this->CellArraySelection->ArrayIsEnabled(name);
  STAT("prevStatus: " << prevStatus)
  STAT_CND_RETURN_PRINT(prevStatus == status, , "no modified")
  if (status)
  {
    this->CellArraySelection->EnableArray(name);
    int newStatus = this->CellArraySelection->ArrayIsEnabled(name);
    STAT("EnableArray newStatus: " << newStatus)
  }
  else
  {
    this->CellArraySelection->DisableArray(name);
    int newStatus = this->CellArraySelection->ArrayIsEnabled(name);
    STAT("DisableArray newStatus: " << newStatus)
  }
  this->Modified();
  STAT("modified")
  STAT_TAC
}

int vtkMockdatasource::GetCellArrayStatus(const char* name)
{
  STAT_TIC("vtkMockdatasource", "GetCellArrayStatus")
  STAT_INPUT("name", name)
  STAT_RETURN_TYPE_PRINT(int, this->CellArraySelection->ArrayIsEnabled(name), "")
}
//--------------------------------------------------------------------------------------------------
int vtkMockdatasource::GetNumberOfMaterialArrays()
{
  STAT_TIC("vtkMockdatasource", "GetNumberOfMaterialArrays")
  STAT_RETURN_TYPE_PRINT(int, this->MaterialArraySelection->GetNumberOfArrays(), "")
}

const char* vtkMockdatasource::GetMaterialArrayName(int index)
{
  STAT_TIC("vtkMockdatasource", "GetMaterialArrayName")
  STAT_INPUT("index", index)
  STAT_RETURN_PRINT(this->MaterialArraySelection->GetArrayName(index),
    std::string(this->MaterialArraySelection->GetArrayName(index)))
}

void vtkMockdatasource::SetMaterialArrayStatus(const char* name, int status)
{
  STAT_TIC("vtkMockdatasource", "SetMaterialArrayStatus")
  STAT_INPUT("name", name)
  STAT_INPUT("status", status)
  int prevStatus = this->MaterialArraySelection->ArrayIsEnabled(name);
  STAT("prevStatus: " << prevStatus)
  STAT_CND_RETURN_PRINT(prevStatus == status, , "no modified")
  if (status)
  {
    this->MaterialArraySelection->EnableArray(name);
    int newStatus = this->MaterialArraySelection->ArrayIsEnabled(name);
    STAT("EnableArray newStatus: " << newStatus)
  }
  else
  {
    this->MaterialArraySelection->DisableArray(name);
    int newStatus = this->MaterialArraySelection->ArrayIsEnabled(name);
    STAT("DisableArray newStatus: " << newStatus)
  }
  this->Modified();
  STAT("modified")
  STAT_TAC
}

int vtkMockdatasource::GetMaterialArrayStatus(const char* name)
{
  STAT_TIC("vtkMockdatasource", "GetMaterialArrayStatus")
  STAT_INPUT("name", name)
  STAT_RETURN_TYPE_PRINT(int, this->MaterialArraySelection->ArrayIsEnabled(name), "")
}
//--------------------------------------------------------------------------------------------------
int vtkMockdatasource::GetNumberOfMeshArrays()
{
  STAT_TIC("vtkMockdatasource", "GetNumberOfMeshArrays")
  STAT_RETURN_TYPE_PRINT(int, this->MeshArraySelection->GetNumberOfArrays(), "")
}

const char* vtkMockdatasource::GetMeshArrayName(int index)
{
  STAT_TIC("vtkMockdatasource", "GetMeshArrayName")
  STAT_INPUT("index", index)
  STAT_RETURN_PRINT(this->MeshArraySelection->GetArrayName(index),
    std::string(this->MeshArraySelection->GetArrayName(index)))
}

void vtkMockdatasource::SetMeshArrayStatus(const char* name, int status)
{
  STAT_TIC("vtkMockdatasource", "SetMeshArrayStatus")
  STAT_INPUT("name", name)
  STAT_INPUT("status", status)
  int prevStatus = this->MeshArraySelection->ArrayIsEnabled(name);
  STAT("prevStatus: " << prevStatus)
  STAT_CND_RETURN_PRINT(prevStatus == status, , "no modified")
  if (status)
  {
    this->MeshArraySelection->EnableArray(name);
    int newStatus = this->MeshArraySelection->ArrayIsEnabled(name);
    STAT("EnableArray newStatus: " << newStatus)
  }
  else
  {
    this->MeshArraySelection->DisableArray(name);
    int newStatus = this->MeshArraySelection->ArrayIsEnabled(name);
    STAT("DisableArray newStatus: " << newStatus)
  }
  this->Modified();
  STAT("modified")
  STAT_TAC
}

int vtkMockdatasource::GetMeshArrayStatus(const char* name)
{
  STAT_TIC("vtkMockdatasource", "GetMeshArrayStatus")
  STAT_INPUT("name", name)
  STAT_RETURN_TYPE_PRINT(int, this->MeshArraySelection->ArrayIsEnabled(name), "")
}
//--------------------------------------------------------------------------------------------------
int vtkMockdatasource::GetNumberOfOptionArrays()
{
  STAT_TIC("vtkMockdatasource", "GetNumberOfOptionArrays")
  STAT_RETURN_TYPE_PRINT(int, this->OptionArraySelection->GetNumberOfArrays(), "")
}

const char* vtkMockdatasource::GetOptionArrayName(int index)
{
  STAT_TIC("vtkMockdatasource", "GetOptionArrayName")
  STAT_INPUT("index", index)
  STAT_RETURN_PRINT(this->OptionArraySelection->GetArrayName(index),
    std::string(this->OptionArraySelection->GetArrayName(index)))
}

void vtkMockdatasource::SetOptionArrayStatus(const char* name, int status)
{
  STAT_TIC("vtkMockdatasource", "SetOptionArrayStatus")
  STAT_INPUT("name", name)
  STAT_INPUT("status", status)
  int prevStatus = this->OptionArraySelection->ArrayIsEnabled(name);
  STAT("prevStatus: " << prevStatus)
  STAT_CND_RETURN_PRINT(prevStatus == status, , "no modified")
  if (status)
  {
    this->OptionArraySelection->EnableArray(name);
    int newStatus = this->OptionArraySelection->ArrayIsEnabled(name);
    STAT("EnableArray newStatus: " << newStatus)
  }
  else
  {
    this->OptionArraySelection->DisableArray(name);
    int newStatus = this->OptionArraySelection->ArrayIsEnabled(name);
    STAT("DisableArray newStatus: " << newStatus)
  }
  this->SetReLoad(true);
  STAT("SetReLoad(true)")
  this->Modified();
  STAT("modified")
  STAT_TAC
}

int vtkMockdatasource::GetOptionArrayStatus(const char* name)
{
  STAT_TIC("vtkMockdatasource", "GetOptionArrayStatus")
  STAT_INPUT("name", name)
  STAT_RETURN_TYPE_PRINT(int, this->OptionArraySelection->ArrayIsEnabled(name), "")
}
//--------------------------------------------------------------------------------------------------
int vtkMockdatasource::FillInputPortInformation(int port, vtkInformation* info)
{
  STAT_TIC("vtkMockdatasource", "FillInputPortInformation")
  info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
  STAT_RETURN_TYPE_PRINT(int, this->Superclass::FillInputPortInformation(port, info), "")
}
//--------------------------------------------------------------------------------------------------
// same HIc GetData.cxx
#define MEMO_TRI(arraySelection)                                                                   \
  {                                                                                                \
    STAT("MEMO_TRI #" << arraySelection->GetNumberOfArrays());                                     \
    std::map<std::string, bool> selectionMap;                                                      \
    std::vector<std::string> selectionVector;                                                      \
    for (std::size_t i = 0; i < arraySelection->GetNumberOfArrays(); ++i)                          \
    {                                                                                              \
      std::string name = arraySelection->GetArrayName(i);                                          \
      selectionVector.emplace_back(name);                                                          \
      selectionMap[name] = arraySelection->ArrayIsEnabled(name.c_str());                           \
    }                                                                                              \
    std::sort(selectionVector.begin(), selectionVector.end());                                     \
    arraySelection->RemoveAllArrays();                                                             \
    for (const auto& it : selectionVector)                                                         \
    {                                                                                              \
      arraySelection->AddArray(it.c_str());                                                        \
      if (!selectionMap[it])                                                                       \
      {                                                                                            \
        arraySelection->DisableArray(it.c_str());                                                  \
      }                                                                                            \
    }                                                                                              \
  }
//--------------------------------------------------------------------------------------------------
int vtkMockdatasource::RequestInformation(vtkInformation* request,
  vtkInformationVector** inputVector,
  vtkInformationVector* outputVector)
{
  STAT_TIC("vtkMockdatasource", "RequestInformation")
  setOptions();

  if (std::string(this->FileName) != this->prevFileName)
  {
    this->m_isFirst = true;
  }

  // Condition to reread the meta-information of the database such
  // as the list of value fields on Point, Cell, the list of meshes or
  // simulation times available.
  bool reload = false;
  if (this->ReLoad)
  {
    STAT("Ask reload explicitly")
    reload = true;
    this->SetReLoad(false);
    STAT("SetReLoad(false)")
    this->Modified();
    STAT("modified")
  }
  else if (std::string(this->FileName) != this->prevFileName)
  {
    STAT("Ask reload by modify filename")
    reload = true;
  }

  if (reload)
  {
    this->m_data_source = nullptr;
  }

  bool ret = true;
  if (!this->m_data_source)
  {
    STAT("Build data new source")
    this->setOptions();
    std::set<std::string> PointArraySelectionSet;
    std::set<std::string> CellArraySelectionSet;
    std::set<std::string> MaterialArraySelectionSet;
    std::set<std::string> MeshArraySelectionSet;
    std::set<std::string> OptionArraySelectionSet;
    if (!this->m_isFirst)
    {

#define MEMO_SET(name_array_selection, array_selection, set_selection)                             \
  STAT("MEMO_SET " << name_array_selection << " #" << array_selection->GetNumberOfArrays());       \
  for (std::size_t i = 0; i < array_selection->GetNumberOfArrays(); ++i)                           \
  {                                                                                                \
    std::string name = array_selection->GetArrayName(i);                                           \
    if (array_selection->ArrayIsEnabled(name.c_str()))                                             \
    {                                                                                              \
      STAT("- " << name << " enable");                                                             \
      set_selection.insert(name);                                                                  \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      STAT("- " << name << " disable");                                                            \
    }                                                                                              \
  }

      MEMO_SET("MESH", this->MeshArraySelection, MeshArraySelectionSet)
      MEMO_SET("MATERIAL", this->MaterialArraySelection, MaterialArraySelectionSet)
      MEMO_SET("CELL", this->CellArraySelection, CellArraySelectionSet)
      MEMO_SET("POINT", this->PointArraySelection, PointArraySelectionSet)
      MEMO_SET("OPTION", this->OptionArraySelection, OptionArraySelectionSet)
    }
    this->PointArraySelection->RemoveAllArrays();
    this->CellArraySelection->RemoveAllArrays();
    this->MaterialArraySelection->RemoveAllArrays();
    this->MeshArraySelection->RemoveAllArrays();
    this->OptionArraySelection->RemoveAllArrays();
    ret = this->m_data_source->GetGlobalInformation(vtkMultiProcessController::GetGlobalController(),
      this->FileName,
      this->m_times,
      this->PointArraySelection,
      this->CellArraySelection,
      this->MaterialArraySelection,
      this->MeshArraySelection,
      this->OptionArraySelection);
    STAT("GetGlobalInformation return " << ret)
    if (!this->m_isFirst)
    {

#define MEMO_GET(name_array_selection, set_selection, array_selection)                             \
  STAT("MEMO_GET " << name_array_selection << " #" << array_selection->GetNumberOfArrays());       \
  for (std::size_t i = 0; i < array_selection->GetNumberOfArrays(); ++i)                           \
  {                                                                                                \
    std::string name = array_selection->GetArrayName(i);                                           \
    if (this->prevFileName == "" || set_selection.find(name) != set_selection.end())               \
    {                                                                                              \
      array_selection->EnableArray(name.c_str());                                                  \
      STAT("- " << name << " enable");                                                             \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      array_selection->DisableArray(name.c_str());                                                 \
      STAT("- " << name << " disable");                                                            \
    }                                                                                              \
  }

      MEMO_GET("MESH", MeshArraySelectionSet, this->MeshArraySelection)
      MEMO_GET("MATERIAL", MaterialArraySelectionSet, this->MaterialArraySelection)
      MEMO_GET("CELL", CellArraySelectionSet, this->CellArraySelection)
      MEMO_GET("POINT", PointArraySelectionSet, this->PointArraySelection)

#define MEMO_GET_WITH_STATUS(name_array_selection, set_selection, array_selection)                 \
  STAT("MEMO_GET " << name_array_selection << " #" << array_selection->GetNumberOfArrays());       \
  for (std::size_t i = 0; i < array_selection->GetNumberOfArrays(); ++i)                           \
  {                                                                                                \
    std::string name = array_selection->GetArrayName(i);                                           \
    if (this->prevFileName == "" || set_selection.find(name) != set_selection.end())               \
    {                                                                                              \
      array_selection->EnableArray(name.c_str());                                                  \
      STAT("- " << name << " enable (heritage)");                                                  \
    }                                                                                              \
    else if (array_selection->ArrayIsEnabled(name.c_str()))                                        \
    {                                                                                              \
      array_selection->EnableArray(name.c_str());                                                  \
      STAT("- " << name << " enable (init)");                                                      \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      array_selection->DisableArray(name.c_str());                                                 \
      STAT("- " << name << " disable (init)");                                                     \
    }                                                                                              \
  }

      MEMO_GET_WITH_STATUS("OPTION", OptionArraySelectionSet, this->OptionArraySelection)
    }
    this->m_isFirst = false;
    MEMO_TRI(this->PointArraySelection)
    MEMO_TRI(this->CellArraySelection)
    MEMO_TRI(this->MaterialArraySelection)
    MEMO_TRI(this->MeshArraySelection)
    MEMO_TRI(this->OptionArraySelection)
    if (reload)
    {
      this->prevFileName = this->FileName;
    }
    this->m_data_source->PrintOptionsTool(this->OptionArraySelection);

    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    outInfo->Set(
      vtkStreamingDemandDrivenPipeline::TIME_STEPS(), this->m_times.data(), this->m_times.size());
    SetAllSimulationTimes();
    if (this->m_times.size() > 1)
    {
      double timeRange[2];
      this->GetTimeRange(timeRange);
      outInfo->Set(vtkStreamingDemandDrivenPipeline::TIME_RANGE(), timeRange, 2);
    }
    outInfo->Set(vtkAlgorithm::CAN_HANDLE_PIECE_REQUEST(), 1);
    if (reload)
    {
      this->prevFileName = this->FileName;
    }
  }

  this->ReLoad = false;

#define ARRAY_SELECTION_PRINT(name_array_selection, array_selection)                               \
  for (int i = 0; i < array_selection->GetNumberOfArrays(); ++i)                                   \
  {                                                                                                \
    std::string name = array_selection->GetArrayName(i);                                           \
    STAT(name_array_selection << " #" << i << " " << name                                          \
                              << " status:" << array_selection->ArrayIsEnabled(name.c_str()))      \
  }

  ARRAY_SELECTION_PRINT("MESH", this->MeshArraySelection)
  ARRAY_SELECTION_PRINT("MATERIAL", this->MaterialArraySelection)
  ARRAY_SELECTION_PRINT("MATERIAL", this->CellArraySelection)
  ARRAY_SELECTION_PRINT("MATERIAL", this->PointArraySelection)

  this->Modified();
  STAT_RETURN_BOOL_PRINT(
    ret && this->Superclass::RequestInformation(request, inputVector, outputVector), "")
}
//--------------------------------------------------------------------------------------------------
int vtkMockdatasource::RequestData(vtkInformation* vtkNotUsed(request),
  vtkInformationVector** inputVector,
  vtkInformationVector* outputVector)
{
  STAT_TIC("vtkMockdatasource", "RequestData")

  this->setOptions();

  vtkIdType nbPointArray = this->PointArraySelection->GetNumberOfArrays();
  vtkIdType nbCellArray = this->CellArraySelection->GetNumberOfArrays();
  STAT("Before #nbPointArray" << this->PointArraySelection->GetNumberOfArrays())
  STAT("       #nbCellArray" << this->CellArraySelection->GetNumberOfArrays())

  int CrtTime = TimeStep;
  if (IndexSimulationTime >= 0)
  {
    CrtTime = IndexSimulationTime;
  }
  STAT("CrtTime:" << CrtTime)

  bool ret = !this->m_data_source->GetData(CrtTime,
    outputVector,
    this->PointArraySelection,
    this->CellArraySelection,
    this->MaterialArraySelection,
    this->MeshArraySelection);

  STAT("After #nbPointArray" << this->PointArraySelection->GetNumberOfArrays())
  STAT("       #nbCellArray" << this->CellArraySelection->GetNumberOfArrays())

  if (nbPointArray != this->PointArraySelection->GetNumberOfArrays() ||
    nbCellArray != this->CellArraySelection->GetNumberOfArrays())
  {
    STAT("Set modify point or cell array selection")
    this->m_modified_point_or_cell_array_selection = true;
  }

  STAT_CND_RETURN_PRINT(ret, 0, "")

  this->m_data_source->PrintToolOutputVector(outputVector);
  STAT_RETURN_TYPE_PRINT(int, 1, "")
}
//--------------------------------------------------------------------------------------------------
int vtkMockdatasource::RequestUpdateExtent(vtkInformation* request,
  vtkInformationVector** inputVector,
  vtkInformationVector* outputVector)
{
  STAT_TIC("vtkMockdatasource", "RequestUpdateExtent")
  vtkInformation* outInfo = outputVector->GetInformationObject(0);
  if (outInfo->Has(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP()))
  {
    double upTime = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP());
    for (std::size_t i = 0; i < this->m_times.size(); ++i)
    {
      if (this->m_times[i] == upTime)
      {
        this->SetTimeStep(i);
        break;
      }
    }
  }
  STAT_RETURN_TYPE_PRINT(
    int, this->Superclass::RequestUpdateExtent(request, inputVector, outputVector), "")
}
//--------------------------------------------------------------------------------------------------
