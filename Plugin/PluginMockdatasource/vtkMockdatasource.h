//
// File vtkMockdatasource.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020-22 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//

#ifndef VTK_MOCK_DATA_SOURCE_H
#define VTK_MOCK_DATA_SOURCE_H

#include <iostream>
#include <map>
#include <string>
#include <vector>

#include <vtkDataArraySelection.h>
#include <vtkMultiBlockDataSetAlgorithm.h>
#include <vtkSmartPointer.h>
#include <vtkStringArray.h>

#include "PluginMockdatasourceModule.h" // for export macro
//--------------------------------------------------------------------------------------------------------------------
class vtkInformation;
class MockDataSourceImpl;
//--------------------------------------------------------------------------------------------------------------------
class PLUGINMOCKDATASOURCE_EXPORT vtkMockdatasource : public vtkMultiBlockDataSetAlgorithm
{
public:
  static vtkMockdatasource* New();

  vtkTypeMacro(vtkMockdatasource, vtkMultiBlockDataSetAlgorithm);
  virtual void PrintSelf(ostream& os, vtkIndent indent) {}

  int CanReadFile(const char* fileName);

  vtkSetStringMacro(FileName);
  vtkGetStringMacro(FileName);

  vtkSetStringMacro(Version); // inutile mais indispensable (PV)
  vtkGetStringMacro(Version);

  void SetSimulationTimes(const char* dimensions);
  vtkGetObjectMacro(AllSimulationTimes, vtkStringArray);
  int IndexSimulationTime = -1;

private:
  vtkSmartPointer<vtkStringArray> AllSimulationTimes;
  void SetAllSimulationTimes()
  {
    this->AllSimulationTimes->SetNumberOfValues(0);
    std::string str_time;
    this->AllSimulationTimes->InsertNextValue(str_time);
    for (unsigned int iSimulationTime = 0; iSimulationTime < this->m_times.size();
         ++iSimulationTime)
    {
      // std::string str_time = std::format("{05d}:{7e}", iSimulationTime,
      //      this->m_times[iSimulationTime]); // C++20 gcc11
      char str[1024];
      sprintf(str, "%5d: %7e", iSimulationTime, this->m_times[iSimulationTime]);
      str_time = std::string(str);
      this->AllSimulationTimes->InsertNextValue(str_time);
    }
  }

public:
  void ActiveReload();
  void ActiveRefreshInformation();

  vtkSetMacro(TimeStep, int);
  vtkGetMacro(TimeStep, int);

  virtual void GetTimeStepRange(int&, int&);
  virtual void GetTimeStepRange(int[2]);
  virtual int* GetTimeStepRange();

  void GetTimeRange(double[2]);

  // vtkSetStringMacro(CurrentTime); // inutile mais indispensable (PV)
  char* GetCurrentTime()
  {
    this->m_current_time = "";
    if (this->m_times.size())
    {
      char str[1024];
      if (IndexSimulationTime >= 0)
      {
        sprintf(str, "%5d: %7e", IndexSimulationTime, this->m_times[IndexSimulationTime]);
      }
      else
      {
        sprintf(str, "%5d: %7e", TimeStep, this->m_times[TimeStep]);
      }
      this->m_current_time = std::string(str);
    }
    return (char*)(this->m_current_time.c_str());
  }

  int GetNumberOfPointArrays();
  const char* GetPointArrayName(int index);
  void SetPointArrayStatus(const char* name, int status);
  int GetPointArrayStatus(const char* name);

  int GetNumberOfCellArrays();
  const char* GetCellArrayName(int index);
  void SetCellArrayStatus(const char* name, int status);
  int GetCellArrayStatus(const char* name);

  int GetNumberOfMaterialArrays();
  const char* GetMaterialArrayName(int index);
  int GetMaterialArrayStatus(const char* name);
  void SetMaterialArrayStatus(const char* name, int status);

  int GetNumberOfMeshArrays();
  const char* GetMeshArrayName(int index);
  int GetMeshArrayStatus(const char* name);
  void SetMeshArrayStatus(const char* name, int status);

  int GetNumberOfOptionArrays();
  const char* GetOptionArrayName(int index);
  int GetOptionArrayStatus(const char* name);
  void SetOptionArrayStatus(const char* name, int status);

  vtkSetMacro(CaptainNumber, int);
  vtkGetMacro(CaptainNumber, int);

protected:
  vtkMockdatasource();
  virtual ~vtkMockdatasource();

  virtual int FillInputPortInformation(int, vtkInformation*);
  virtual int RequestInformation(vtkInformation* request,
    vtkInformationVector** inputVector,
    vtkInformationVector* outputVector);
  virtual int RequestData(vtkInformation* request,
    vtkInformationVector** inputVector,
    vtkInformationVector* outputVector);
  virtual int RequestUpdateExtent(vtkInformation*, vtkInformationVector**, vtkInformationVector*);

private:
  vtkMockdatasource(const vtkMockdatasource&);            // Not implemented
  vtkMockdatasource& operator=(const vtkMockdatasource&); // Not implemented

  bool m_isFirst = true;
  char* FileName = nullptr;

  std::string m_version;
  char* Version = nullptr;

  bool Refresh = false;
  vtkSetMacro(Refresh, bool);
  vtkGetMacro(Refresh, bool);

  bool ReLoad = false;
  vtkSetMacro(ReLoad, bool);
  vtkGetMacro(ReLoad, bool);

  std::string prevFileName;
  int TimeStep = 0;
  std::vector<double> m_times;

  std::string m_current_time;
  char* CurrentTime = nullptr; // inutile mais indispensable (PV)

  bool m_modified_point_or_cell_array_selection = false;
  vtkSmartPointer<vtkDataArraySelection> PointArraySelection;
  vtkSmartPointer<vtkDataArraySelection> CellArraySelection;
  vtkSmartPointer<vtkDataArraySelection> MaterialArraySelection;
  vtkSmartPointer<vtkDataArraySelection> MeshArraySelection;

  vtkSmartPointer<vtkDataArraySelection> OptionArraySelection;
  std::map<std::string, bool> m_bool_option;
  std::map<std::string, bool> m_previous_bool_option;

  const char* CaptainName = "Captain";
  int CaptainNumber = 0;

  std::map<std::string, int> m_int_option;
  std::map<std::string, int> m_previous_int_option;

  void setOptions();

  int m_pe_nb = 1;
  int m_pe_rank = 0;

  std::shared_ptr<MockDataSourceImpl> m_data_source;

  //--------------------------------------------------------------------------------------------------------------------
  // Copy of STAT_CLASS of vtkMacroReaders.h (forbidden multi-heritage)
  mutable std::string m_stat_para = "";
  mutable bool m_stat_para_tracer = true;
  mutable bool m_stat_first = true;
  mutable bool m_tracing = false;
  mutable bool m_stat_chatter = true;
  mutable bool m_stat_chatter_fine = true;
  mutable std::string m_pattern_fct;
  mutable bool m_tracing_warning = true;

  void SetStatPara(const std::string& _stat_para) const { m_stat_para = _stat_para; }
  const std::string GetStatPara(void) const { return m_stat_para; }

  void DisableStatParaTracer() const { m_stat_para_tracer = false; }
  bool GetStatParaTracer() const { return m_stat_para_tracer; }

  static bool Match(bool _tracing, const std::string& _str, std::string _pattern)
  {
    if (_pattern[_pattern.length() - 1] == '*')
    {
      _pattern = _pattern.substr(0, _pattern.length() - 1);
      if (_pattern == "")
      {
        return true;
      }
      if (_str.length() < _pattern.length())
      {
        return false;
      }
      return _str.substr(0, _pattern.length()) == _pattern;
    }
    return (_str == _pattern);
  }

  static void InitChatterStatic(bool _tracing,
    const std::string& _clss,
    bool& _stat_chatter,
    bool& _stat_chatter_fine,
    std::string& _pattern_fct)
  {
    _stat_chatter = false;
    _stat_chatter_fine = false;
    _pattern_fct = "*";
    _stat_chatter = valueVarEnvBoolean("STAT_STATUS");
    if (std::getenv("STAT_ON"))
    {
      _stat_chatter = true;
    }
    const char* var_env = std::getenv("STAT");
    if (_tracing)
    {
      std::cerr << "### InitChatterStatic STAT=\"" << std::string(var_env) << "\"" << std::endl;
    }
    if (var_env)
    {
      std::string tokens(var_env);
      // List tokens
      size_t pos = 0;
      std::vector<std::string> list_tokens;
      while ((pos = tokens.find(";")) != std::string::npos)
      {
        std::string token = tokens.substr(0, pos);
        if (token == "")
        {
          continue;
        }
        tokens = tokens.substr(pos + 1);
        list_tokens.emplace_back(token);
      }
      list_tokens.emplace_back(tokens);
      // Use tokens
      for (const auto& token : list_tokens)
      {
        if (_tracing)
        {
          std::cerr << "### InitChatterStatic token=" << token << std::endl;
        }
        _pattern_fct = "*";
        bool state = true;
        std::size_t deb = 0;
        switch (token[0])
        {
          case '-':
          case '~':
          {
            state = false;
            deb = 1;
            break;
          }
          case '+':
          {
            state = true;
            deb = 1;
            break;
          }
          default:
          {
            state = true;
            deb = 0;
            break;
          }
        }
        pos = token.find(":");
        std::string pattern = token.substr(deb, pos - deb);
        if (pos != std::string::npos)
        {
          // case <clss>:<fct>
          _pattern_fct = token.substr(pos + 1);
          pos = _pattern_fct.find(":");
          if (pos != std::string::npos)
          {
            if (pos != 0)
            {
              std::cout << "### WARNING ### Ignored token: " << token
                        << " in STAT. The token cannot include \":\" or \"::\" once as a separator "
                           "between the name of the class and the name of the function."
                        << std::endl;
              continue;
            }
            // case <clss>::<fct>
            _pattern_fct = _pattern_fct.substr(1);
          }
        }
        if (Match(_tracing, _clss, pattern))
        {
          _stat_chatter = state;
          return;
        }
      }
    }
    _pattern_fct = "*";
    return;
  }

  static bool valueVarEnvBoolean(const std::string& _env)
  {
    if (_env == "")
    {
      return false;
    }
    const char* val = std::getenv(_env.c_str());
    if (val == nullptr)
    {
      return false;
    }
    std::string sval(val);
    const std::map<std::string, bool> know = { { "", true },
      { "1", true },
      { "on", true },
      { "On", true },
      { "true", true },
      { "True", true },
      { "0", false },
      { "off", false },
      { "Off", false },
      { "false", false },
      { "False", false } };
    const auto& it = know.find(sval);
    if (it == know.end())
    {
      std::cerr << "### WARNING ### Ignored environment variable: " << _env
                << " cause non-interpretable value " << sval
                << " (nothing,1,on,On,true,True for true or"
                << " 0,off,Off,false,False)" << std::endl;
      return false;
    }
    bool tracing = false;
    if (std::string(_env) == "STAT_VERBOSE")
    {
      tracing = it->second;
    }
    else
    {
      tracing = valueVarEnvBoolean("STAT_VERBOSE");
    }
    if (tracing)
    {
      std::cout << "Boolean environement variable " << _env << " is " << it->second << std::endl;
    }
    return it->second;
  }

public:
  void InitChatter(const std::string& _clss,
    const std::string& _fct,
    bool& _stat_chatter,
    bool& _stat_chatter_fine) const
  {
    bool _tracing = this->m_tracing;
    if (this->m_stat_first)
    {
      _tracing = valueVarEnvBoolean("STAT_VERBOSE");
      if (std::getenv("STAT_VERBOSE"))
      {
        std::cout << "   STAT_VERBOSE activated" << std::endl;
      }
      if (std::getenv("STAT_ON"))
      {
        std::cout << "   STAT_ON activated \"" << std::getenv("STAT_ON") << "\"" << std::endl;
      }
      if (std::getenv("STAT"))
      {
        std::cout << "   STAT activated \"" << std::getenv("STAT") << "\"" << std::endl;
      }
      if (std::getenv("STAT_WARNING"))
      {
        std::cout << "   STAT_WARNING activated" << std::endl;
        this->m_tracing_warning = valueVarEnvBoolean("STAT_WARNING");
      }
      this->m_stat_first = false;
      this->m_tracing = valueVarEnvBoolean("STAT_VERBOSE");
      InitChatterStatic(this->m_tracing,
        _clss,
        this->m_stat_chatter,
        this->m_stat_chatter_fine,
        this->m_pattern_fct);
    }
    bool ret1 = (this->m_stat_chatter && this->m_pattern_fct == "*");
    bool ret2 = (this->m_stat_chatter && Match(this->m_tracing, _fct, this->m_pattern_fct));
    bool ret3 = (!this->m_stat_chatter && !Match(this->m_tracing, _fct, this->m_pattern_fct));
    _stat_chatter = ret1 || ret2 || ret3;
    _stat_chatter_fine = this->m_stat_chatter_fine;
  }
};
//--------------------------------------------------------------------------------------------------------------------
#endif // VTK_MOCK_DATA_SOURCE_H
//--------------------------------------------------------------------------------------------------------------------
