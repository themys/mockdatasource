//
// File SetOptions.cxx
//
// Created by Jacques-Bernard Lekien.
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "MockDataSourceImpl.h"
//--------------------------------------------------------------------------------------------------
void MockDataSourceImpl::SetOptions(const std::map<std::string, bool>& _bool_option,
  const std::map<std::string, int>& _int_option)
{
  STAT_TIC("MockDataSourceImpl", "SetOptions")

  bool status_api_hic = false;
  const auto& it = this->m_options_bool.find(CHANGED_STATUS_WITH_RELOAD);
  if (it != this->m_options_bool.end() && it->second)
  {
    status_api_hic = true;
    STAT("MEMO " << CHANGED_STATUS_WITH_RELOAD << " " << status_api_hic)
  }

  this->m_options_bool.clear();
  for (const auto& [key, value] : _bool_option)
  {
    this->m_options_bool[key] = value;
    STAT("option " << key << " " << value)
  }

  if (status_api_hic)
  {
    this->m_options_bool[CHANGED_STATUS_WITH_RELOAD] = status_api_hic;
    STAT("SET " << CHANGED_STATUS_WITH_RELOAD << " " << status_api_hic)
  }

  this->m_options_int = _int_option;
  STAT_TAC
}
//--------------------------------------------------------------------------------------------------
