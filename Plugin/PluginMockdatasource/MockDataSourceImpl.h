//
// File MockDataSourceImpl.h
//
// Created by Jacques-Bernard Lekien.
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
#ifndef VTK_MOCK_DATA_SOURCE_IMPL_H
#define VTK_MOCK_DATA_SOURCE_IMPL_H
//--------------------------------------------------------------------------------------------------
#include "../Common/MacroReaders.h"
//--------------------------------------------------------------------------------------------------
#include <map>
#include <memory>
#include <string>
#include <vector>
//--------------------------------------------------------------------------------------------------
#include <vtkAlgorithm.h>
#include <vtkDataArraySelection.h>
#include <vtkInformationVector.h>
#include <vtkMPICommunicator.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkMultiProcessController.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
//--------------------------------------------------------------------------------------------------
// TODO Faire un point h commun
// In "options" parameters of GetGlobalInformation and GetData methods
// Double definition : vtkMockDataSource.cxx, MockDataSourceImpl.cxx
#define MEMORY_EFFICIENT "Memory efficient"
#define CHANGED_STATUS_WITH_RELOAD "Changed with reload"
//--------------------------------------------------------------------------------------------------
class MockDataSourceImpl : public STAT_CLASS
{
public:
  MockDataSourceImpl(const std::map<std::string, bool>& _options_bool,
    const std::map<std::string, int>& _options_int)
    : m_options_bool(_options_bool)
    , m_options_int(_options_int)
  {
    STAT_TIC("MockDataSourceImpl", "MockDataSourceImpl") STAT_TAC
  }

  virtual ~MockDataSourceImpl()
  {
    STAT_TIC("MockDataSourceImpl", "~MockDataSourceImpl")
    this->m_mbds = nullptr;
    STAT_TAC
  }

  virtual void SetAlgorithm(vtkAlgorithm* _algorithm) { this->m_algorithm = _algorithm; }

  void SetOptions(const std::map<std::string, bool>& _bool_option,
    const std::map<std::string, int>& _int_option);

  virtual bool CanReadFile(const char* fileName);

  virtual bool GetGlobalInformation(vtkMultiProcessController *_Controller,
    const char* fileName,
    std::vector<double>& _times,
    vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _OptionArraySelection);

  virtual bool MeshGlobalInformation(vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _OptionArraySelection,
    bool _forced);

  void PrintOptionsTool(vtkSmartPointer<vtkDataArraySelection> _OptionArraySelection);
  void PrintTool(std::string nameArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _ArraySelection);

  virtual bool GetData(int _time,
    vtkInformationVector* _outputVector,
    vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
    vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection);

  void PrintToolOutputVector(vtkInformationVector* _outputVector);

protected:
  std::string m_filename;
  std::map<std::string, bool> m_options_bool;
  std::map<std::string, int> m_options_int;

  bool m_did_get_global_information = false;

  // le nom est impose et commun avec Hercule Services
  const std::string m_global_material_name = "global";

  int m_last_time = -1;

  int m_proc_nb = 1;
  int m_proc_rank = 0;
  vtkMPICommunicator* m_mpi = nullptr;

  // for mock
  virtual vtkSmartPointer<vtkUnstructuredGrid> GetMaterial(int _time,
    int _piece_ind,
    int _block_ind);
  virtual vtkSmartPointer<vtkUnstructuredGrid> GetGlobalMaterial(int _time,
    int _piece_ind,
    int _block_nb);

  vtkSmartPointer<vtkMultiBlockDataSet> m_mbds = nullptr;

  vtkAlgorithm* m_algorithm = nullptr;

  std::map<std::string, long> m_fielddata_int;
  std::map<std::string, double> m_fielddata_double;
  std::map<std::string, std::string> m_fielddata_string;
  void CreateFieldData(vtkDataObject* _do);

private:
  bool GetFielDouble(vtkDataSetAttributes* _geom_datas,
    vtkSmartPointer<vtkDataArraySelection> _ArraySelection,
    std::string _name,
    std::size_t _nb);
};
//--------------------------------------------------------------------------------------------------
#endif // VTK_MOCK_DATA_SOURCE_IMPL_H
