//
// File MockDataSource.cxx
//
// Created by Jacques-Bernard Lekien.
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "MockDataSourceImpl.h"
//--------------------------------------------------------------------------------------------------
#include "../Common/MacroReaders.h"
//--------------------------------------------------------------------------------------------------
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkDataObject.h>
#include <vtkDataSetAttributes.h>
#include <vtkDoubleArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkIntArray.h>
#include <vtkMultiPieceDataSet.h>
#include <vtkPentagonalPrism.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkStringArray.h>
//--------------------------------------------------------------------------------------------------
#include "CreateFieldData.cxx"
//--------------------------------------------------------------------------------------------------
void MockDataSourceImpl::SetOptions(const std::map<std::string, bool>& _bool_option,
  const std::map<std::string, int>& _int_option)
{
  STAT_TIC("MockDataSourceImpl", "SetOptions")

  bool status_api_hic = false;
  const auto& it = this->m_options_bool.find(CHANGED_STATUS_WITH_RELOAD);
  if (it != this->m_options_bool.end() && it->second)
  {
    status_api_hic = true;
    STAT("MEMO " << CHANGED_STATUS_WITH_RELOAD << " " << status_api_hic)
  }

  this->m_options_bool.clear();
  for (const auto& [key, value] : _bool_option)
  {
    this->m_options_bool[key] = value;
    STAT("option " << key << " " << value)
  }

  if (status_api_hic)
  {
    this->m_options_bool[CHANGED_STATUS_WITH_RELOAD] = status_api_hic;
    STAT("SET " << CHANGED_STATUS_WITH_RELOAD << " " << status_api_hic)
  }

  this->m_options_int = _int_option;
  STAT_TAC
}
//--------------------------------------------------------------------------------------------------
bool MockDataSourceImpl::CanReadFile(const char* fileName)
{
  STAT_TIC("MockDataSourceImpl", "CanReadFile")
  this->m_filename = fileName;
  STAT_RETURN_BOOL_PRINT(true, "mock data source selected")
}
//--------------------------------------------------------------------------------------------------
bool MockDataSourceImpl::GetGlobalInformation(
  vtkMultiProcessController *_Controller,
  const char* fileName,
  std::vector<double>& _times,
  vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _OptionArraySelection)
{
  STAT_TIC("MockDataSourceImpl", "GetGlobalInformation")
  this->m_did_get_global_information = false;
  if (!CanReadFile(fileName))
  {
    STAT("m_did_get_global_information " << this->m_did_get_global_information)
    STAT_RETURN_BOOL_PRINT(false, "")
  }

  STAT("Controller")
  this->m_proc_nb = 1;
  this->m_proc_rank = 0;
  this->m_mpi = nullptr;
  if (_Controller)
  {
    STAT("Exist controller")
    this->m_proc_nb = _Controller->GetNumberOfProcesses();
    this->m_proc_rank = _Controller->GetLocalProcessId();
    STAT_SET_PARA(this->m_proc_rank, this->m_proc_nb)
    this->m_mpi = vtkMPICommunicator::SafeDownCast(_Controller->GetCommunicator());
  }

  STAT("Mode mock data source")
  // List of reading options
  this->m_options_bool.clear();
  this->m_options_bool[MEMORY_EFFICIENT] = false;
  this->m_options_bool[CHANGED_STATUS_WITH_RELOAD] = false;
  // TODO this->m_options_bool["generates unexist identifiers"] = true;
  // Time list
  _times = { 1., 2., 3., 4., 5., 6., 7., 8. };
  // List of names of value fields at points
  _PointArraySelection->AddArray("pointData");
  _PointArraySelection->AddArray("pointData2");
  // List of names of value fields at cells
  _CellArraySelection->AddArray("cellData");
  _CellArraySelection->AddArray("cellData2");
  // List of names of sub-meshes of mesh
  _MaterialArraySelection->AddArray("meshA");
  _MaterialArraySelection->AddArray("meshB");
  // Specific sub-mesh of mesh
  _MaterialArraySelection->AddArray(this->m_global_material_name.c_str());
  _MaterialArraySelection->DisableArray(this->m_global_material_name.c_str());
  // List of names of meshes
  _MeshArraySelection->AddArray("mesh");
  // Automagic allocation mechanism after initialisation
  for (const auto& [key, value] : this->m_options_bool)
  {
    _OptionArraySelection->AddArray(key.c_str());
    if (value)
    {
      _OptionArraySelection->EnableArray(key.c_str());
    }
    else
    {
      _OptionArraySelection->DisableArray(key.c_str());
    }
  }
  PrintOptionsTool(_OptionArraySelection);
  this->m_did_get_global_information = true;
  STAT("m_did_get_global_information " << this->m_did_get_global_information)
  STAT_RETURN_BOOL_PRINT(true, "")
}
//--------------------------------------------------------------------------------------------------
bool MockDataSourceImpl::MeshGlobalInformation(
  vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _OptionArraySelection,
  bool _forced)
{
  STAT_TIC("MockDataSourceImpl", "MeshGlobalInformation")
  STAT("m_did_get_global_information " << this->m_did_get_global_information)
  STAT_CND_RETURN_BOOL_PRINT(!this->m_did_get_global_information, false, "");
  STAT("Mode mock data source: not effect")
  STAT_RETURN_BOOL_PRINT(true, "")
}
//--------------------------------------------------------------------------------------------------
void MockDataSourceImpl::PrintOptionsTool(
  vtkSmartPointer<vtkDataArraySelection> _OptionArraySelection)
{
  STAT_TIC("MockDataSourceImpl", "PrintOptionsTool")
  for (std::size_t option_ind = 0; option_ind < _OptionArraySelection->GetNumberOfArrays();
       ++option_ind)
  {
    std::string option_name = _OptionArraySelection->GetArrayName(option_ind);
    this->m_options_bool[option_name] = _OptionArraySelection->ArrayIsEnabled(option_name.c_str());
    STAT("Option bool name: " << option_name << "]: " << this->m_options_bool[option_name]);
  }
  for (const auto& [key, value] : m_options_int)
  {
    STAT("Option int name: " << key << "]: " << value)
  }
  STAT_TAC
}
//--------------------------------------------------------------------------------------------------
void MockDataSourceImpl::PrintTool(std::string nameArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _ArraySelection)
{
  STAT_TIC("MockDataSourceImpl", "PrintTool")
  STAT(nameArraySelection)
  for (std::size_t i = 0; i < _ArraySelection->GetNumberOfArrays(); ++i)
  {
    std::string name = _ArraySelection->GetArrayName(i);
    STAT("- " << name << " " << _ArraySelection->ArrayIsEnabled(name.c_str()));
  }
  STAT_TAC
}
//--------------------------------------------------------------------------------------------------
vtkSmartPointer<vtkUnstructuredGrid> MockDataSourceImpl::GetMaterial(int _time,
  int _piece_ind,
  int _mat_ind)
{
  STAT_TIC("MockDataSourceImpl", "GetMaterial")
  int numberOfVertices = 0;
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkUnstructuredGrid> uGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();

  double tr[3] = { 5. * _piece_ind + 5 * _time * _mat_ind, 5. * _mat_ind, 0. };
  // https://vtk.org/Wiki/VTK/Examples/Cxx/GeometricObjects/Cell3DDemonstration
  /*
        int numberOfVertices = 14;
        int numberOfFaces = 9;
        int numberOfFaceVertices = 4;
        vtkIdType heptagonPointsIds[14] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
        vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
        points->InsertNextPoint(.6 + tr[0], .534, 1.1009301757813);  // id 0
        points->InsertNextPoint(.6 + tr[0], .545, 1.1500146484375);  // id 1
        points->InsertNextPoint(.6 + tr[0], .556, 1.2255187988281);   // id 2
        points->InsertNextPoint(1.1 + tr[0], .556, 1.6478076171875); // id 3
        points->InsertNextPoint(1.1 + tr[0], .534, 1.7926538085938); // id 4
        points->InsertNextPoint(1.1 + tr[0], .545, 1.7166479492188); // id 5
        points->InsertNextPoint(.85 + tr[0], .534, 1.0092297363281); // id 6
        points->InsertNextPoint(.6 + tr[0], .534, 5.1009301757813);  // id 7
        points->InsertNextPoint(.6 + tr[0], .545, 5.1500146484375);  // id 8
        points->InsertNextPoint(.6 + tr[0], .556, 5.2255187988281);  // id 9
        points->InsertNextPoint(1.1 + tr[0], .556, 5.6478076171875); // id 10
        points->InsertNextPoint(1.1 + tr[0], .534, 5.7926538085938); // id 11
        points->InsertNextPoint(1.1 + tr[0], .545, 5.7166479492188); // id 12
        points->InsertNextPoint(.85 + tr[0], .534, 5.0092297363281); // id13
        vtkIdType heptagonPrismFace[9][4] = {
          { 0, 1, 7, 8 },
          { 1, 2, 8, 9 },
          { 2, 3, 9, 10 },
          { 3, 4, 10, 11 },
          { 4, 5, 11, 12 },
          { 5, 6, 12, 13 },
          { 6, 1, 13, 8 },
          { 0, 1, 2, 3 },
          { 7, 8, 9, 10 },
        };
        vtkSmartPointer<vtkCellArray> heptagonFaces = vtkSmartPointer<vtkCellArray>::New();
        for (int i = 0; i < numberOfFaces; ++i)
        {
          heptagonFaces->InsertNextCell(numberOfFaceVertices, heptagonPrismFace[i]);
        }

        vtkSmartPointer<vtkUnstructuredGrid> uGrid =
     vtkSmartPointer<vtkUnstructuredGrid>::New(); uGrid->InsertNextCell(VTK_POLYHEDRON,
          numberOfVertices,
          heptagonPointsIds,
          numberOfFaces,
          heptagonFaces->GetPointer()); // Ne compile pas ???
        uGrid->SetPoints(points);
  */
  points->InsertNextPoint(11 + tr[0], 10 + tr[1], 10 + tr[2]);
  ++numberOfVertices;
  points->InsertNextPoint(13 + tr[0], 10 + tr[1], 10 + tr[2]);
  ++numberOfVertices;
  points->InsertNextPoint(14 + tr[0], 12 + tr[1], 10 + tr[2]);
  ++numberOfVertices;
  points->InsertNextPoint(12 + tr[0], 14 + tr[1], 10 + tr[2]);
  ++numberOfVertices;
  points->InsertNextPoint(10 + tr[0], 12 + tr[1], 10 + tr[2]);
  ++numberOfVertices;
  points->InsertNextPoint(11 + tr[0], 10 + tr[1], 14 + tr[2]);
  ++numberOfVertices;
  points->InsertNextPoint(13 + tr[0], 10 + tr[1], 14 + tr[2]);
  ++numberOfVertices;
  points->InsertNextPoint(14 + tr[0], 12 + tr[1], 14 + tr[2]);
  ++numberOfVertices;
  points->InsertNextPoint(12 + tr[0], 14 + tr[1], 14 + tr[2]);
  ++numberOfVertices;
  points->InsertNextPoint(10 + tr[0], 12 + tr[1], 14 + tr[2]);
  ++numberOfVertices;

  vtkSmartPointer<vtkPentagonalPrism> pentagonalPrism = vtkSmartPointer<vtkPentagonalPrism>::New();
  for (int i = 0; i < numberOfVertices; ++i)
  {
    pentagonalPrism->GetPointIds()->SetId(i, i);
  }

  uGrid->SetPoints(points);
  uGrid->InsertNextCell(pentagonalPrism->GetCellType(), pentagonalPrism->GetPointIds());
  STAT_RETURN_PRINT(uGrid, "uGrid")
}
//--------------------------------------------------------------------------------------------------
vtkSmartPointer<vtkUnstructuredGrid> MockDataSourceImpl::GetGlobalMaterial(int _time,
  int _piece_ind,
  int _block_nb)
{
  STAT_TIC("MockDataSourceImpl", "GetGlobalMaterial")
  vtkSmartPointer<vtkUnstructuredGrid> uGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
  int numberOfVertices = 0;
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  for (int mat_ind = 0; mat_ind < _block_nb - 1; ++mat_ind)
  {
    double tr[3] = { 5. * _piece_ind + 5 * _time * mat_ind, 5. * mat_ind, 0. };

    points->InsertNextPoint(11 + tr[0], 10 + tr[1], 10 + tr[2]);
    ++numberOfVertices;
    points->InsertNextPoint(13 + tr[0], 10 + tr[1], 10 + tr[2]);
    ++numberOfVertices;
    points->InsertNextPoint(14 + tr[0], 12 + tr[1], 10 + tr[2]);
    ++numberOfVertices;
    points->InsertNextPoint(12 + tr[0], 14 + tr[1], 10 + tr[2]);
    ++numberOfVertices;
    points->InsertNextPoint(10 + tr[0], 12 + tr[1], 10 + tr[2]);
    ++numberOfVertices;
    points->InsertNextPoint(11 + tr[0], 10 + tr[1], 14 + tr[2]);
    ++numberOfVertices;
    points->InsertNextPoint(13 + tr[0], 10 + tr[1], 14 + tr[2]);
    ++numberOfVertices;
    points->InsertNextPoint(14 + tr[0], 12 + tr[1], 14 + tr[2]);
    ++numberOfVertices;
    points->InsertNextPoint(12 + tr[0], 14 + tr[1], 14 + tr[2]);
    ++numberOfVertices;
    points->InsertNextPoint(10 + tr[0], 12 + tr[1], 14 + tr[2]);
    ++numberOfVertices;

    vtkSmartPointer<vtkPentagonalPrism> pentagonalPrism =
      vtkSmartPointer<vtkPentagonalPrism>::New();
    for (int i = 0; i < 10; ++i)
    {
      pentagonalPrism->GetPointIds()->SetId(i, numberOfVertices - 10 + i);
    }

    uGrid->InsertNextCell(pentagonalPrism->GetCellType(), pentagonalPrism->GetPointIds());
  }
  uGrid->SetPoints(points);
  STAT_RETURN_PRINT(uGrid, "uGrid")
}
//--------------------------------------------------------------------------------------------------
bool MockDataSourceImpl::GetFielDouble(vtkDataSetAttributes* _geom_datas,
  vtkSmartPointer<vtkDataArraySelection> _ArraySelection,
  std::string _name,
  std::size_t _nb)
{
  STAT_TIC("MockDataSourceImpl", "getCreateField")
  bool status = _ArraySelection->ArrayIsEnabled(_name.c_str());
  STAT("status:" << status)
  vtkDataArray* field_data = _geom_datas->GetArray(_name.c_str());
  if (!status)
  {
    if (field_data)
    {
      STAT("Remove point field data: " << _name)
      _geom_datas->RemoveArray(_name.c_str());
    }
  }
  else if (field_data)
  {
    STAT("Already exist point field data: " << _name)
  }
  else
  {
    vtkSmartPointer<vtkDoubleArray> field_data = vtkSmartPointer<vtkDoubleArray>::New();
    field_data->SetNumberOfComponents(1);
    field_data->SetName(_name.c_str());
    field_data->SetNumberOfTuples(_nb);
    _geom_datas->AddArray(field_data);
    for (int iVal = 0; iVal < _nb; ++iVal)
    {
      field_data->InsertValue(iVal, 42. + iVal * 2.);
    }
    STAT("Create point field data: " << _name)
  }
  STAT_RETURN_BOOL_PRINT(true, "")
}
//--------------------------------------------------------------------------------------------------
bool MockDataSourceImpl::GetData(int _time,
  vtkInformationVector* _outputVector,
  vtkSmartPointer<vtkDataArraySelection> _PointArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _CellArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _MaterialArraySelection,
  vtkSmartPointer<vtkDataArraySelection> _MeshArraySelection)
{
  STAT_TIC("MockDataSourceImpl", "GetData")
  //
  STAT_CND_RETURN_BOOL_PRINT(
    !this->m_did_get_global_information, false, "Call GetGlobalInformation before this call.")

  if (m_last_time != _time)
  {
    m_last_time = _time;
    if (this->m_mbds)
    {
      STAT("Delete MBDS")
      this->m_mbds = nullptr;
    }
  }
  vtkInformation* outInfo = _outputVector->GetInformationObject(0);

  vtkMultiBlockDataSet* outMultiBlockDataSet =
    vtkMultiBlockDataSet::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  std::size_t meshes_nb = _MeshArraySelection->GetNumberOfArrays();

  // uniq material id by name
  std::map<std::string, std::size_t> materials_ind;
  for (std::size_t mat_ind = 0; mat_ind < _MaterialArraySelection->GetNumberOfArrays(); ++mat_ind)
  {
    materials_ind[_MaterialArraySelection->GetArrayName(mat_ind)] = mat_ind;
  }

  // school case with all meshes as
  std::map<std::string, std::size_t> meshes_materials_nb;
  for (std::size_t mesh_ind = 0; mesh_ind < meshes_nb; ++mesh_ind)
  {
    meshes_materials_nb[_MeshArraySelection->GetArrayName(mesh_ind)] =
      _MaterialArraySelection->GetNumberOfArrays();
  }

  if (this->m_mbds == nullptr)
  {
    STAT("Initialize Field Data key and value")
    this->m_fielddata_int.clear();
    this->m_fielddata_int["cycle"] = _time * 3;

    this->m_fielddata_double.clear();
    this->m_fielddata_double["dt"] = 10.42;
    this->m_fielddata_double["time"] = _time * 1.1;

    this->m_fielddata_string.clear();
    this->m_fielddata_string["study"] = "school case";

    STAT("Create MBDS")
    this->m_mbds = vtkSmartPointer<vtkMultiBlockDataSet>::New();

    STAT("SetNumberOfBlocks/Meshes " << meshes_nb)
    this->m_mbds->SetNumberOfBlocks(meshes_nb);

    for (std::size_t mesh_ind = 0; mesh_ind < meshes_nb; ++mesh_ind)
    {
      std::string name = std::string(_MeshArraySelection->GetArrayName(mesh_ind)) + " (no loaded)";
      STAT("Set name block in MBDS " << this->m_mbds << " Mesh#" << mesh_ind << ": " << name)
      this->m_mbds->GetMetaData(mesh_ind)->Set(vtkCompositeDataSet::NAME(), name.c_str());
    }
  }
  else
  {
    STAT("Reuse MBDS")
    STAT_ASSERT(this->m_mbds->GetNumberOfBlocks() == meshes_nb, "");
  }

  int piece_ind = this->m_proc_rank;
  int piece_nb = this->m_proc_nb;

  for (std::size_t mesh_ind = 0; mesh_ind < meshes_nb; ++mesh_ind)
  {
    std::string mesh_name = _MeshArraySelection->GetArrayName(mesh_ind);
    STAT("Mesh#" << mesh_ind << " " << mesh_name)
    if (!_MeshArraySelection->ArrayIsEnabled(mesh_name.c_str()))
    {
      STAT("Mesh status is disable.")
      vtkDataObject* mesh_block = this->m_mbds->GetBlock(mesh_ind);
      if (!mesh_block)
      {
        STAT("Nothing to do (delete).")
        continue;
      }
      STAT("Delete")
      std::string name = std::string(_MeshArraySelection->GetArrayName(mesh_ind)) + " (no loaded)";
      STAT("Set name block in MBDS " << this->m_mbds << " Mesh#" << mesh_ind << ": " << name
                                     << " with nullptr")
      this->m_mbds->GetMetaData(mesh_ind)->Set(vtkCompositeDataSet::NAME(), name.c_str());
      this->m_mbds->SetBlock(mesh_ind, nullptr);
      continue;
    }
    STAT("Mesh status is enable.")
    vtkDataObject* mesh_block = this->m_mbds->GetBlock(mesh_ind);
    if (mesh_block)
    {
      STAT("Nothing to do (build).")
    }
    else
    {
      STAT("Build")
      std::string name = std::string(_MeshArraySelection->GetArrayName(mesh_ind));
      STAT("Set name block in MBDS " << this->m_mbds << " Mesh#" << mesh_ind << ": " << name)
      this->m_mbds->GetMetaData(mesh_ind)->Set(vtkCompositeDataSet::NAME(), name.c_str());

      std::size_t meshes_nb = _MaterialArraySelection->GetNumberOfArrays();

      vtkSmartPointer<vtkMultiBlockDataSet> mat_mbds = vtkSmartPointer<vtkMultiBlockDataSet>::New();
      this->m_mbds->SetBlock(mesh_ind, mat_mbds);

      std::size_t mat_nb = meshes_materials_nb[mesh_name];

      STAT("SetNumberOfBlocks/Materials " << mat_nb)
      mat_mbds->SetNumberOfBlocks(mat_nb);

      for (std::size_t mat_ind = 0; mat_ind < mat_nb; ++mat_ind)
      {
        std::string name =
          std::string(_MaterialArraySelection->GetArrayName(mat_ind)) + " (no loaded)";
        STAT("Set name block in MBDS " << this->m_mbds << " Mesh#" << mesh_ind << " Material#"
                                       << mat_ind << ": " << name)
        mat_mbds->GetMetaData(mat_ind)->Set(vtkCompositeDataSet::NAME(), name.c_str());
      }
    }

    vtkMultiBlockDataSet* mesh_mbds =
      vtkMultiBlockDataSet::SafeDownCast(this->m_mbds->GetBlock(mesh_ind));
    STAT_ASSERT(mesh_mbds, "")

    std::size_t mat_nb = meshes_materials_nb[mesh_name];

    STAT_ASSERT(mat_nb == mesh_mbds->GetNumberOfBlocks(), "")

    for (std::size_t mat_ind = 0; mat_ind < mat_nb; ++mat_ind)
    {
      const char* block_name = _MaterialArraySelection->GetArrayName(mat_ind);

      int status = _MaterialArraySelection->ArrayIsEnabled(block_name);

      STAT("block ind:" << mat_ind << " name:" << std::string(block_name) << " status: " << status)

      vtkMultiPieceDataSet* mpds = vtkMultiPieceDataSet::SafeDownCast(mesh_mbds->GetBlock(mat_ind));
      STAT(mpds)
      if (!status)
      {
        if (!mpds)
        {
          STAT("   delete (nothing)")
          continue;
        }
        mesh_mbds->SetBlock(mat_ind, nullptr);
        STAT("   delete")
        continue;
      }
      if (mpds)
      {
        STAT("   build (nothing)")
      }
      else
      {
        STAT("   build")
        vtkSmartPointer<vtkMultiPieceDataSet> sp_mpds = vtkSmartPointer<vtkMultiPieceDataSet>::New();
        STAT("SetNumberOfPieces " << piece_nb)
        sp_mpds->SetNumberOfPieces(piece_nb);
        char piece_name[1024];
        sprintf(piece_name, "server_%d", piece_ind);
        STAT("SetPiece[#" << piece_ind << "] MetaData: " << std::string(piece_name))
        sp_mpds->GetMetaData(piece_ind)->Set(vtkCompositeDataSet::NAME(), piece_name);

        STAT("SetBlock[#" << mat_ind << "] MetaData:" << std::string(block_name))
        mesh_mbds->GetMetaData(mat_ind)->Set(vtkCompositeDataSet::NAME(), block_name);
        STAT("SetBlock[#" << mat_ind << ", MetaData:" << std::string(block_name) << "]")
        mesh_mbds->SetBlock(mat_ind, sp_mpds);

        STAT("SetPiece[#" << piece_ind << " MetaData: " << std::string(piece_name) << "]: ")
        vtkSmartPointer<vtkUnstructuredGrid> sp_uGrid;
        if (this->m_global_material_name == block_name)
        {
          sp_uGrid = this->GetGlobalMaterial(_time, piece_ind, mat_nb);
          STAT("New global material")
        }
        else
        {
          sp_uGrid = this->GetMaterial(_time, piece_ind, mat_ind);
          STAT("New material")
        }

        sp_mpds->SetPiece(piece_ind, sp_uGrid);

        this->m_fielddata_int["vtkMaterialId"] = materials_ind[block_name];
        CreateFieldData(sp_uGrid);
      }

      vtkSmartPointer<vtkMultiPieceDataSet> sp_mpds =
        vtkMultiPieceDataSet::SafeDownCast(mesh_mbds->GetBlock(mat_ind));
      vtkUnstructuredGrid* uGrid = vtkUnstructuredGrid::SafeDownCast(sp_mpds->GetPiece(piece_ind));

      vtkPointData* point_data = uGrid->GetPointData();
      for (std::size_t iPointField = 0; iPointField < _PointArraySelection->GetNumberOfArrays();
           ++iPointField)
      {
        const char* name = _PointArraySelection->GetArrayName(iPointField);
        int status = _PointArraySelection->ArrayIsEnabled(name);
        vtkDataArray* field = point_data->GetArray(name);
        if (status)
        {
          if (!field)
          {
            GetFielDouble(point_data, _PointArraySelection, name, uGrid->GetNumberOfPoints());
          }
        }
        else
        {
          if (field)
          {
            STAT("Remove point data array " << name)
            point_data->RemoveArray(name);
          }
        }
      }
      vtkCellData* cell_data = uGrid->GetCellData();
      for (std::size_t iCellField = 0; iCellField < _CellArraySelection->GetNumberOfArrays();
           ++iCellField)
      {
        const char* name = _CellArraySelection->GetArrayName(iCellField);
        int status = _CellArraySelection->ArrayIsEnabled(name);
        vtkDataArray* field = cell_data->GetArray(name);
        if (status)
        {
          if (!field)
          {
            GetFielDouble(cell_data, _CellArraySelection, name, uGrid->GetNumberOfCells());
          }
        }
        else
        {
          if (field)
          {
            STAT("Remove cell data array " << name)
            cell_data->RemoveArray(name);
          }
        }
      }
    }
  }
  outMultiBlockDataSet->ShallowCopy(this->m_mbds);

  STAT_RETURN_BOOL_PRINT(true, "")
}
//--------------------------------------------------------------------------------------------------
void MockDataSourceImpl::PrintToolOutputVector(vtkInformationVector* _outputVector)
{
  STAT_TIC("MockDataSourceImpl", "ToolPrint")
  vtkInformation* outInfo = _outputVector->GetInformationObject(0);
  vtkSmartPointer<vtkMultiBlockDataSet> multiblock = this->m_mbds;
  STAT("multiblock:" << multiblock);
  if (this->m_mbds)
  {
    STAT("nbBlocks:" << multiblock->GetNumberOfBlocks());
    for (int iBlock = 0; iBlock < multiblock->GetNumberOfBlocks(); ++iBlock)
    {
      auto* block = multiblock->GetBlock(iBlock);
      STAT("  block#" << iBlock);
      auto* multipiece = vtkMultiPieceDataSet::SafeDownCast(block);
      STAT("  multipiece:" << multipiece);
      if (multipiece)
      {
        STAT("  nbPieces:" << multipiece->GetNumberOfPieces());
        for (int iPiece = 0; iPiece < multipiece->GetNumberOfPieces(); ++iPiece)
        {
          auto* piece = multipiece->GetPieceAsDataObject(iPiece);
          STAT("    piece#" << iPiece << ":" << piece);
          vtkUnstructuredGrid* uGrid = vtkUnstructuredGrid::SafeDownCast(piece);
          STAT("    vtkUnstructuredGrid:" << uGrid);
          if (uGrid)
          {
            STAT("    #points:" << uGrid->GetNumberOfPoints());
            STAT("    #cells:" << uGrid->GetNumberOfCells());
          }
        }
      }
    }
  }
}
//--------------------------------------------------------------------------------------------------
