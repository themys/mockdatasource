//
// File MockDataSource.cxx
//
// Created by Jacques-Bernard Lekien.
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
//--------------------------------------------------------------------------------------------------
#include "MockDataSourceImpl.h"
//--------------------------------------------------------------------------------------------------
#include "../Common/MacroReaders.h"
//--------------------------------------------------------------------------------------------------
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkDataObject.h>
#include <vtkDataSetAttributes.h>
#include <vtkDoubleArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkIntArray.h>
#include <vtkMultiPieceDataSet.h>
#include <vtkPentagonalPrism.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkStringArray.h>
//--------------------------------------------------------------------------------------------------
void MockDataSourceImpl::CreateFieldData(vtkDataObject* _do)
{
  STAT_TIC("MockDataSourceImpl", "CreateFieldData")

  vtkFieldData* field_data = _do->GetFieldData();

  for (const auto& [key, value] : this->m_fielddata_int)
  {
    STAT("Create field data: " << key)
    vtkSmartPointer<vtkIntArray> data = vtkSmartPointer<vtkIntArray>::New();
    data->SetNumberOfComponents(1);
    data->SetName(key.c_str());
    data->SetNumberOfTuples(1);
    field_data->AddArray(data);
    data->InsertValue(0, value);
    STAT("Create block field data: " << key << " " << value)
  }

  for (const auto& [key, value] : this->m_fielddata_double)
  {
    STAT("Create field data: " << key)
    vtkSmartPointer<vtkDoubleArray> data = vtkSmartPointer<vtkDoubleArray>::New();
    data->SetNumberOfComponents(1);
    data->SetName(key.c_str());
    data->SetNumberOfTuples(1);
    field_data->AddArray(data);
    data->InsertValue(0, value);
    STAT("Create block field data: " << key << " " << value)
  }

  for (const auto& [key, value] : this->m_fielddata_string)
  {
    STAT("Create field data: " << key)
    vtkSmartPointer<vtkStringArray> data = vtkSmartPointer<vtkStringArray>::New();
    data->SetNumberOfComponents(1);
    data->SetName(key.c_str());
    data->SetNumberOfTuples(1);
    field_data->AddArray(data);
    data->InsertValue(0, value.c_str());
    STAT("Create block field data: " << key << " " << value)
  }

  STAT_TAC
}
//--------------------------------------------------------------------------------------------------
