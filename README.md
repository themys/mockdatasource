MockDataSource simulates a reader filter.

Create an empty file **HDep-.**

[[_TOC_]]

# Test 1
## Procedure
1. Run PV
2. Open this file.
3. All disable **Mesh Array**, **Material Array**, **Cell Data Array** and **Point Data Array**.
4. **Apply**
5. **Delete**
6. Close PV
## Normal Result 
Nothing, it's normal.

*Information* tab : vtkMBDS with one named block :
- **mesh (no loaded)**
## Bug Result
1. *MBDS Inspector* tab : nothing ! Why ?
2. If I all enable **Cell Data Array**, **Point Data Array**, **Material Array** and **Mesh Array** before doing **Apply**, there is only one call to *vtkMockdatasource::RequestInformation*, why?
## Leaks

```
vtkDebugLeaks has detected LEAKS!
in GetData : Class "vtkMultiBlockDataSet" has 1 instance still around.
in         : Class "vtkInformationStringValue" has 1 instance still around.
in         : Class "vtkMPIController" has 1 instance still around.
in         : Class "vtkFieldData" has 1 instance still around.
in         : Class "vtkInformation" has 2 instances still around.
```
### Leaks vtkMultiBlockDataSet
```bash
export VTK_DEBUG_LEAKS_TRACE_CLASSES=vtkMultiBlockDataSet
```
```
MockDataSourceImpl::GetData
vtkMockdatasource::RequestData
```

### Leaks vtkInformationStringValue
```bash
export VTK_DEBUG_LEAKS_TRACE_CLASSES=vtkInformationStringValue
```

```
MockDataSourceImpl::GetData
vtkMockdatasource::RequestData
``` 

### Leaks vtkMPIController
```bash
export VTK_DEBUG_LEAKS_TRACE_CLASSES=vtkMPIController
```

```
vtkProcessModule::Initialize
vtkInitializationHelper::Initialize
pqApplicationCore::pqApplicationCore
pqPVApplicationCore::pqPVApplicationCore
``` 

### Leaks vtkFieldData
```bash
export VTK_DEBUG_LEAKS_TRACE_CLASSES=vtkFieldData
```

```
vtkDataObject::vtkDataObject
vtkCompositeDataSet::vtkCompositeDataSet()
vtkDataObjectTree::vtkDataObjectTree
vtkMultiBlockDataSet::vtkMultiBlockDataSet
vtkMultiBlockDataSet::New
MockDataSourceImpl::GetData
vtkMockdatasource::RequestData
```

### Leaks vtkInformation
```bash
export VTK_DEBUG_LEAKS_TRACE_CLASSES=vtkInformation
```

```
vtkDataObject::vtkDataObject
vtkCompositeDataSet::vtkCompositeDataSet()
vtkDataObjectTree::vtkDataObjectTree
vtkMultiBlockDataSet::vtkMultiBlockDataSet
vtkMultiBlockDataSet::New
MockDataSourceImpl::GetData
vtkMockdatasource::RequestData
```

# Test 2
## Procedure
Same *Test 1* but step 3 : 
- All enable **Mesh Array**.
- All disable **Material Array**, **Cell Data Array** and **Point Data Array**

## Normal Result 
Nothing, it's normal.

*Information* tab : vtkMBDS with one named block
- **mesh (no loaded)**

itself a vtkMBDS of three named blocks :
- **global (no loaded)**
- **matA (no loaded)**
- **matB (no loaded)**
## Bug Result
Same constat test 1.
1. *MBDS Inspector* tab : nothing ! Why ?
2. If I all enable **Cell Data Array**, **Point Data Array**, **Material Array** and **Mesh Array** before doing **Apply**, there is only one call to *vtkMockdatasource::RequestInformation*, why?
## Leaks
```
vtkDebugLeaks has detected LEAKS!
Class "vtkMultiBlockDataSet" has 2 instances still around.
Class "vtkInformationStringValue" has 4 instances still around.
Class "vtkMPIController" has 1 instance still around.
Class "vtkFieldData" has 2 instances still around.
Class "vtkInformation" has 6 instances still around.
```

# Test 3
## Procedure
Same *Test 1* but step 3 : 
- All enable **Mesh Array**.
- In **Material Array** : enable **matA** and **matB**, disable **global**.
- All disable **Cell Data Array**, **Point Data Array**

## Normal Result 
Nothing, it's normal.

*Information* tab : vtkMBDS with one named block
- **mesh (no loaded)**

itself a vtkMBDS of three named blocks :
- **global (no loaded)**
- **matA**
- **matB**

*MBDS Inspector* tab : same *Information tab*
## Bug Result
Nothing
## Leaks
```
vtkDebugLeaks has detected LEAKS!
Class "vtkInformationVector" has 16 instances still around.
Class "vtkPointData" has 2 instances still around.
Class "vtkTypeFloat64Array" has 2 instances still around.
Class "vtkInformation" has 44 instances still around.
Class "vtkInformationIntegerValue" has 8 instances still around.
Class "9vtkBufferIdE" has 6 instances still around.
Class "vtkPoints" has 4 instances still around.
Class "vtkInformationDoubleVectorValue" has 28 instances still around.
Class "vtkIntArray" has 2 instances still around.
Class "vtkFloatArray" has 2 instances still around.
Class "vtkStringArray" has 2 instances still around.
Class "9vtkBufferIhE" has 4 instances still around.
Class "9vtkBufferIfE" has 2 instances still around.
Class "vtkIdTypeArray" has 4 instances still around.
Class "vtkMPIController" has 1 instance still around.
Class "vtkIdList" has 4 instances still around.
Class "vtkCommand or subclass" has 2 instances still around.
Class "9vtkBufferIxE" has 8 instances still around.
Class "vtkEmptyCell" has 2 instances still around.
Class "vtkTypeInt64Array" has 4 instances still around.
Class "vtkCellArray" has 2 instances still around.
Class "vtkDoubleArray" has 4 instances still around.
Class "vtkUnstructuredGrid" has 2 instances still around.
Class "vtkUnsignedCharArray" has 4 instances still around.
Class "9vtkBufferIiE" has 2 instances still around.
Class "vtkCellData" has 2 instances still around.
Class "vtkFieldData" has 6 instances still around.
Class "vtkMultiBlockDataSet" has 2 instances still around.
Class "vtkInformationStringValue" has 6 instances still around.
Class "vtkMultiPieceDataSet" has 2 instances still around.
Class "vtkCellTypes" has 2 instances still around.
```
# Documentation
## VTK Debug Leaks
https://discourse.vtk.org/t/new-feature-getting-a-stack-trace-on-leaked-objects-with-vtk-debug-leaks/835
